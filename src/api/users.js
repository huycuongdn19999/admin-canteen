import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/user`;

export function getUsersPerPage(data) {
  const endpoint = `${baseEndpoint}?itemPerPage=${data.limit}&page=${data.page}`;
  return request(endpoint, "GET");
}

export function getUserById(data) {
  const endpoint = `${baseEndpoint}/${data.userId}`;
  return request(endpoint, "GET");
}

export function updateUser(data) {
  const endpoint = `${baseEndpoint}/${data.userId}`;
  return request(endpoint, "PUT", data.data);
}

export function delUser(data) {
  const endpoint = `${baseEndpoint}/${data.userId}`;
  return request(endpoint, "DELETE");
}
