import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/category`;

export function getCategorysPerPage(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}

export function getCategoryById(data) {
  const endpoint = `${baseEndpoint}/${data.categoryId}`;
  return request(endpoint, "GET");
}

export function postCategory(data) {
  const endpoint = `${baseEndpoint}`;
  var formData = new FormData();
  formData.append("catetogory_name", data.catetogory_name);
  formData.append("description", data.description);
  formData.append("image", data.image[0]);
  return request(endpoint, "POST", formData);
}

export function updateCategory(data) {
  const endpoint = `${baseEndpoint}/${data.categoryId}`;
  var formData = new FormData();
  formData.append("catetogory_name", data.catetogory_name);
  formData.append("description", data.description);
  formData.append("image", data.image[0]);
  return request(endpoint, "PUT", formData);
}

export function delCategory(data) {
  const endpoint = `${baseEndpoint}/${data.categoryId}`;
  return request(endpoint, "DELETE");
}
