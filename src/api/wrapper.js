import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/user`;

export function changePassword(data) {
  const endpoint = `${baseEndpoint}/changePassword`;
  return request(endpoint, "PUT", data);
}
