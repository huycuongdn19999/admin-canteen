import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/contact`;

export function getContactsPerPage(data) {
  const endpoint = `${baseEndpoint}?itemPerPage=${data.limit}&page=${data.page}`;
  return request(endpoint, "GET");
}

export function delContact(data) {
  const endpoint = `${baseEndpoint}/${data.contactId}`;
  return request(endpoint, "DELETE");
}
