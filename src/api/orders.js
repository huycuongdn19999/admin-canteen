import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/order`;

export function getOrdersPerPage(data) {
  const endpoint = `${baseEndpoint}?itemPerPage=${data.limit}&&page=${data.page}`;
  return request(endpoint, "GET");
}

export function getOrderById(data) {
  const endpoint = `${baseEndpoint}/${data.orderId}`;
  return request(endpoint, "GET");
}

export function postOrder(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function updateOrder(data) {
  const endpoint = `${config.apiBaseURL}/status/${data.data.status}/${data.orderId} `;
  return request(endpoint, "PUT");
}

export function delOrder(data) {
  const endpoint = `${baseEndpoint}/${data.orderId}`;
  return request(endpoint, "DELETE");
}