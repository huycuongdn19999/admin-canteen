import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/recruitment`;

export function getRecruitInfo(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}

export function updateRecruitInfo(data) {
  const endpoint = `${baseEndpoint}/TUYEN_DUNG`;
  return request(endpoint, "PUT", data);
}
