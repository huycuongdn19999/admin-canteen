import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/code`;

export function getDiscountsPerPage(data) {
  const endpoint = `${baseEndpoint}?itemPerPage=${data.limit}&&page=${data.page}`;
  return request(endpoint, "GET");
}

export function getDiscountById(data) {
  const endpoint = `${baseEndpoint}/${data.discountId}`;
  return request(endpoint, "GET");
}

export function postDiscount(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function updateDiscount(data) {
  const endpoint = `${baseEndpoint}/${data.discountId}`;
  return request(endpoint, "PUT", data.data);
}

export function delDiscount(data) {
  const endpoint = `${baseEndpoint}/${data.discountId}`;
  return request(endpoint, "DELETE");
}
