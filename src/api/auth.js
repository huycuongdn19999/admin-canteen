import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/login`;

export function login(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}
