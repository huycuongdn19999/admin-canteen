import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/product`;

// export function getProductsPerPage(data) {
//   const endpoint = `${baseEndpoint}?itemPerPage=${data.limit}&page=${data.page}`;
//   return request(endpoint, "GET");
// }
export function getProductsPerPage(data) {
  console.log("get product list");
  return {
    success: true,
    pagination: {
      totalItem: 9999,
      itemPerPage: 9999,
    },

    data: [
      {
        image: "",
        name_product: "Ca chien",
        catetogory_name: "aaaa",
        price: 333,
        unit: "fasdf",
      },
    ],
  };
}

export function getProductById(data) {
  const endpoint = `${baseEndpoint}/${data.productId}`;
  return request(endpoint, "GET");
}

export function postProduct(data) {
  const endpoint = `${baseEndpoint}`;
  var formData = new FormData();
  formData.append("unit", data.unit);
  formData.append("categoryId", data.categoryId);
  formData.append("price", data.price);
  formData.append("name_product", data.name_product);
  formData.append("image", data.image);
  formData.append("desc", "desc");
  return request(endpoint, "POST", formData);
}

export function updateProduct(data) {
  const endpoint = `${baseEndpoint}/${data.productId}`;
  var formData = new FormData();
  formData.append("unit", data.unit);
  formData.append("categoryId", data.categoryId);
  formData.append("price", data.price);
  formData.append("name_product", data.name_product);
  formData.append("desc", "desc");
  if (data.hasOwnProperty("image")) {
    formData.append("image", data.image);
  }
  return request(endpoint, "PUT", formData);
}

export function delProduct(data) {
  const endpoint = `${baseEndpoint}/${data.productId}`;
  return request(endpoint, "DELETE");
}
