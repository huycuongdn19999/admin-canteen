import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/post`;

export function getBlogsPerPage(data) {
  const endpoint = `${baseEndpoint}?itemPerPage=${data.limit}&&page=${data.page}`;
  return request(endpoint, "GET");
}

export function getBlogById(data) {
  const endpoint = `${baseEndpoint}/${data.blogId}`;
  return request(endpoint, "GET");
}

export function postBlog(data) {
  const endpoint = `${baseEndpoint}`;
  var formData = new FormData();
  formData.append("title", data.title);
  formData.append("content", data.content);
  formData.append("image", data.image);
  return request(endpoint, "POST", formData);
}

export function updateBlog(data) {
  const endpoint = `${baseEndpoint}/${data.blogId}`;
  var formData = new FormData();
  formData.append("title", data.title);
  formData.append("content", data.content);
  formData.append("image", data.image);
  return request(endpoint, "PUT", formData);
}

export function delBlog(data) {
  const endpoint = `${baseEndpoint}/${data.blogId}`;
  return request(endpoint, "DELETE");
}
