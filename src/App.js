import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "connected-react-router";
import configureStore from "./stores";
import Wrapper from "modules/Wrapper/components/WrapperContainer";
import LoginPage from "modules/LoginPage/components/LoginPageContainer";
import ProductsPage from "modules/ProductsPage/components/ProductsPageContainer";
import BlogPage from "modules/BlogPage/components/BlogPageContainer";
import DiscountPage from "modules/DiscountPage/components/DiscountPageContainer";
import UserPage from "modules/UserPage/components/UserPageContainer";
import ContactPage from "modules/ContactPage/components/ContactPageContainer";
import RecruitPage from "modules/RecruitPage/components/RecruitPageContainer";
import OrderPage from "modules/OrderPage/components/OrderPageContainer";
import Page404 from "modules/common/404page";

const { store, history } = configureStore();

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Provider store={store}>
          <ConnectedRouter history={history}>
            <Switch>
              <Route path="/product" exact component={ProductsPage} />
              {/* <Route path="/login" exact component={LoginPage} />
              <Route path="/404" exact component={Page404} />
              <Switch>
                <Wrapper>
                  <Route path="/" exact component={ProductsPage} />
                  <Route path="/blog" exact component={BlogPage} />
                  <Route path="/discount-code" exact component={DiscountPage} />
                  <Route path="/user" exact component={UserPage} />
                  <Route path="/order" exact component={OrderPage} />
                  <Route path="/contact" exact component={ContactPage} />
                  <Route path="/recruit" exact component={RecruitPage} />
                  <Route
                    path="*"
                    render={props => {
                      if (
                        ![
                          "/login",
                          "/",
                          "/blog",
                          "/discount-code",
                          "/user",
                          "/order",
                          "/contact",
                          "/recruit"
                        ].includes(props.location.pathname)
                      ) {
                        return <Redirect from="*" to="/404" />;
                      }
                    }}
                  />
                </Wrapper>
              </Switch> */}
            </Switch>
          </ConnectedRouter>
        </Provider>
      </React.Fragment>
    );
  }
}

export default App;
