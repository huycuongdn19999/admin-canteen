import axios from "axios";
import store from "store";
import _ from "lodash";


export const handerError = async (error, requestData) => {
  return new Promise(async (resolve, reject) => {
    const status = _.get(error, "response.status");
    if (status < 500 && status >= 400) {
      return resolve(_.get(error, "response", ""));
    } else {
      return resolve(_.get(error, "response", ""));
    }
  });
};

export const CONTENT_TYPE = {
  form: "application/x-www-form-urlencoded",
  json: "application/json"
};

export const request = (
  endpoint,
  method,
  data,
  config = false,
  access_token = null
) => {
  return new Promise((resolve, reject) => {
    const getHeaders = (contentType = CONTENT_TYPE.json) => {
      const token = access_token ? access_token : store.get("access_token");
      const header = {
        authorization: token,
        "Content-Type": contentType
      };
      return header;
    };

    const headers = !config ? getHeaders() : getHeaders(CONTENT_TYPE.form);
    const options = {
      method,
      url: endpoint,
      headers,
      data: method !== "GET" ? data : null,
      params: method === "GET" ? data : null
    };
    return axios(options)
      .then(res => {
        return resolve(res);
      })
      .catch(async error => {
        try {
          return resolve(
            await handerError(error, { endpoint, method, data, config })
          );
        } catch (err) {
          return reject(error);
        }
      });
  });
};
