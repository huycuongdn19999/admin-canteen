/**
 * @file reducers
 */

import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

// Place for reducers' app
import ProductsPage, { name as nameOfProductsPage } from "modules/ProductsPage";
import DiscountPage, { name as nameOfDiscountPage } from "modules/DiscountPage";
import ContactPage, { name as nameOfContactPage } from "modules/ContactPage";
import RecruitPage, { name as nameOfRecruitPage } from "modules/RecruitPage";
import OrderPage, { name as nameOfOrderPage } from "modules/OrderPage";
import LoginPage, { name as nameOfLoginPage } from "modules/LoginPage";
import BlogPage, { name as nameOfBlogPage } from "modules/BlogPage";
import UserPage, { name as nameOfUserPage } from "modules/UserPage";
import Wrapper, { name as nameOfWrapper } from "modules/Wrapper";

const reducers = {
  [nameOfProductsPage]: ProductsPage,
  [nameOfDiscountPage]: DiscountPage,
  [nameOfRecruitPage]: RecruitPage,
  [nameOfContactPage]: ContactPage,
  [nameOfLoginPage]: LoginPage,
  [nameOfOrderPage]: OrderPage,
  [nameOfBlogPage]: BlogPage,
  [nameOfUserPage]: UserPage,
  [nameOfWrapper]: Wrapper
};

export default history =>
  combineReducers({
    ...reducers,
    router: connectRouter(history)
  });
