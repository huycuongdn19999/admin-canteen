/**
 * @file init sagas
 */

import { all } from "redux-saga/effects";

// Place for sagas' app
import { sagas as ProductsPage } from "modules/ProductsPage";
import { sagas as BlogPage } from "modules/BlogPage";
import { sagas as DiscountPage } from "modules/DiscountPage";
import { sagas as UserPage } from "modules/UserPage";
import { sagas as OrderPage } from "modules/OrderPage";
import { sagas as ContactPage } from "modules/ContactPage";
import { sagas as RecruitPage } from "modules/RecruitPage";
import { sagas as LoginPage } from "modules/LoginPage";
import { sagas as Wrapper } from "modules/Wrapper";

/*----Saga Root List-----------------*/
let sagasList = [
  ProductsPage(),
  DiscountPage(),
  RecruitPage(),
  ContactPage(),
  LoginPage(),
  OrderPage(),
  BlogPage(),
  UserPage(),
  Wrapper()
];

export default function* rootSaga(getState) {
  yield all(sagasList);
}
