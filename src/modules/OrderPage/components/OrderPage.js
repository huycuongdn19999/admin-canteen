import React, { Component } from "react";
import ModalUpdateOrder from "./ModalUpdateOrder";
import ModalConfirmDel from "../../common/ModalConfirmDel";
import Header from "../../common/Header";
import OrderList from "./OrderList";
import { Container, Row } from "reactstrap";

class OrderPage extends Component {
  componentDidMount() {
    this.props.actions.getAllOrders({ limit: 10, page: 1 });
  }

  render() {
    const { curOrder, updateOrderModal, delOrderModal } = this.props;
    return (
      <React.Fragment>
        <div className="block-header">
          <div className="block-header__container">
            <Header
              title={"Danh sách đơn hàng"}
              subTitle={"Đơn hàng"}
              active={"Danh sách"}
            />
          </div>
        </div>
        <Container fluid={true}>
          <Row className="clearfix">
            <OrderList {...this.props} />
          </Row>
        </Container>
        <ModalUpdateOrder
          open={updateOrderModal}
          toggleModal={() =>
            this.props.actions.toggleModal({ type: "updateOrderModal" })
          }
          props={this.props}
        />
        <ModalConfirmDel
          open={delOrderModal}
          toggleModal={() =>
            this.props.actions.toggleModal({
              type: "delOrderModal"
            })
          }
          title={"Xác nhận xoá đơn hàng này"}
          desc="Bạn có chắc chắn xoá đơn hàng này"
          actions={() => {
            this.props.actions.delOrder({ orderId: curOrder._id });
            this.props.actions.toggleModal({
              type: "delOrderModal"
            });
          }}
        />
      </React.Fragment>
    );
  }
}

export default OrderPage;
