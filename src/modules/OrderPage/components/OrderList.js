import React, { Component } from "react";
import LoadingScreen from "../../common/LoadingScreen";
import DataTable from "react-data-table-component";
import { urlExFormat } from "services/urlFormat";
import classnames from "classnames";
import memoize from "memoize-one";
import { Col } from "reactstrap";
import _ from "lodash";

const customStyles = {
  headCells: {
    style: {
      backgroundColor: "#e47297",
      color: "#ffffff",
      border: "none",
      fontWeight: 600
    }
  }
};

const columns = memoize(props => [
  {
    name: "Mã đơn hàng",
    selector: "_id",
    grow: 2
  },
  {
    name: "Địa chỉ",
    selector: "address"
  },
  {
    name: "Điện thoại",
    selector: "phone"
  },
  {
    name: "Số tiền",
    selector: "total_money",
    sortable: true
  },
  {
    name: "Trạng thái",
    selector: "status",
    sortable: true,
    cell: (row, index) => {
      let statusBadge;
      if (row.status === "Chưa giao") {
        statusBadge = "badge-warning";
      } else if (row.status === "Đã giao") {
        statusBadge = "badge-success";
      } else {
        statusBadge = "badge-default";
      }
      return (
        <span className={classnames("badge", statusBadge)}>{row.status}</span>
      );
    }
  },
  {
    name: "Công cụ",
    center: true,
    cell: (row, index) => (
      <div>
        <span
          className="btn btn-primary waves-effect waves-float btn-sm waves-light-blue"
          onClick={() => {
            props.actions.handleCurOrder(row);
          }}
        >
          <i className="zmdi zmdi-edit"></i>
        </span>
        <span
          className="btn btn-danger waves-effect waves-float btn-sm waves-pink"
          onClick={() => {
            props.actions.toggleModal({ type: "delOrderModal", data: row });
          }}
        >
          <i className="zmdi zmdi-delete"></i>
        </span>
      </div>
    )
  }
]);

const ExpandedComponent = ({ data }) => {
  let { cart } = data;
  return (
    <div className="expanded-container">
      <div className="table-responsive cart-listing">
        <table className="table">
          <thead>
            <tr>
              <th>Tất cả sản phẩm</th>
              <th>Giá</th>
              <th>Số lượng</th>
              <th>Thành tiền</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {_.isEmpty(cart) ? (
              <tr>
                <p>Không có sản phẩm nào trong giỏ hàng</p>
              </tr>
            ) : (
              cart.map((item, index) => {
                return (
                  <tr>
                    <td>
                      <div className="cart_thumnail">
                        <img
                          src={urlExFormat(item.image)}
                          alt={item.name_product}
                        />
                        <span>{item.name_product}</span>
                      </div>
                    </td>
                    <td>{item.price}</td>
                    <td>{item.qty}</td>
                    <td>
                      <span className="total-row">
                        {item.price * item.qty} Đ
                      </span>
                    </td>
                  </tr>
                );
              })
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
};

class OrderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      limit: 10
    };
  }

  componentDidUpdate(prevProps) {
    const { limit, page } = this.state;
    const { isDelOrderSuccess } = this.props;
    if (
      prevProps.isDelOrderSuccess !== isDelOrderSuccess &&
      isDelOrderSuccess
    ) {
      this.props.actions.getAllOrders({
        page: Number.parseInt(page),
        limit
      });
    }
  }

  _handlePageChange = page => {
    const { limit } = this.state;
    this.props.actions.getAllOrders({
      page: Number.parseInt(page),
      limit
    });
    this.setState({
      page
    });
  };

  _handlePerRowsChange = row => {
    const { page } = this.state;
    this.props.actions.getAllOrders({
      page: Number.parseInt(page),
      limit: row
    });
  };

  render() {
    const { listOrders, pagination, isLoading } = this.props;
    return (
      <Col lg="12">
        <div className="card">
          <DataTable
            data={listOrders}
            columns={columns(this.props)}
            highlightOnHover
            noHeader={true}
            customStyles={customStyles}
            pagination
            paginationServer={true}
            paginationTotalRows={pagination.totalItem}
            onChangeRowsPerPage={this._handlePerRowsChange}
            onChangePage={this._handlePageChange}
            progressPending={isLoading}
            progressComponent={<LoadingScreen />}
            expandableRows
            expandOnRowClicked
            expandableRowsComponent={<ExpandedComponent />}
            noDataComponent="Hiện không có đơn hàng nào"
          />
        </div>
      </Col>
    );
  }
}

export default OrderList;
