import React, { useEffect } from "react";
import { FormGroup, FormFeedback, Label, Input } from "reactstrap";
import { useForm } from "react-hook-form";

export default function App(props) {
  const { register, errors, handleSubmit, setValue } = useForm();
  const { isUpdate, curOrder } = props;

  useEffect(() => {
    if (isUpdate) {
      setValue([{ code: curOrder.code }, { Order: curOrder.Order }]);
    }
  }, [setValue, isUpdate, curOrder.code, curOrder.Order]);

  const onSubmit = (data, e) => {
    if (isUpdate) {
      props.actions.updateOrder({
        data,
        orderId: curOrder._id
      });
    }
    setTimeout(() => {
      e.target.reset();
    }, 500);
  };

  return (
    <div className="card">
      <div className="body">
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormGroup>
            <Label for="select">Trạng thái đơn hàng</Label>
            <Input
              type="select"
              name="status"
              id="select"
              innerRef={register({ required: "Vui lòng không để trống" })}
              invalid={errors.status ? true : false}
            >
              <option value="not-delivery">Chưa giao</option>
              <option value="delivery">Đã giao</option>
              <option value="cancelled">Huỷ đơn</option>
            </Input>
            <FormFeedback>
              {errors.status && errors.status.message}
            </FormFeedback>
          </FormGroup>
          <FormGroup className="mt-2 text-right">
            <button
              className="btn btn-raised btn-primary waves-effect"
              type="submit"
            >
              SUBMIT
            </button>
            &nbsp;
            <button
              className="btn btn-raised btn-default waves-effect"
              onClick={e => {
                e.preventDefault();
                props.actions.toggleModal({ type: "updateOrderModal" });
              }}
            >
              Đóng
            </button>
          </FormGroup>
        </form>
      </div>
    </div>
  );
}
