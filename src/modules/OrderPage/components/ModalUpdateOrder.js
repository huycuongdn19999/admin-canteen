import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import UpdateOrderForm from "./UpdateOrderForm";

const modalUpdateOrder = ({ open, toggleModal, props }) => {
  return (
    <Modal isOpen={open}>
      <ModalHeader toggle={toggleModal}>Thông tin mã giảm giá</ModalHeader>
      <ModalBody>
        <UpdateOrderForm {...props} />
      </ModalBody>
    </Modal>
  );
};

export default modalUpdateOrder;
