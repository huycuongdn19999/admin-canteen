import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as ordersAPI from "api/orders";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

/* ORDERS */

export function* handleGetAllOrders(action) {
  try {
    let res = yield call(ordersAPI.getOrdersPerPage, action.payload);
    yield put(actions.getAllOrdersSuccess(res.data));
  } catch (err) {
    yield put(actions.getAllOrdersFail(err));
  }
}

export function* handleUpdateOrder(action) {
  try {
    let res = yield call(ordersAPI.updateOrder, action.payload);
    NotificationManager.success(
      "Cập nhật đơn hàng thành công",
      "Thông báo",
      2000
    );
    yield put(actions.updateOrderSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.updateOrderFail(err));
  }
}

export function* handleDelOrder(action) {
  try {
    let res = yield call(ordersAPI.delOrder, action.payload);
    NotificationManager.success(
      "Xoá đơn hàng thành công",
      "Thông báo",
      2000
    );
    yield put(actions.delOrderSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.delOrderFail(err));
  }
}

/*----------------------------------------------------------*/

/* ORDERS */

export function* getAllOrders() {
  yield takeAction(actions.getAllOrders, handleGetAllOrders);
}
export function* updateOrder() {
  yield takeAction(actions.updateOrder, handleUpdateOrder);
}
export function* delOrder() {
  yield takeAction(actions.delOrder, handleDelOrder);
}
/*----------------------------------------------------------*/
export default [getAllOrders, updateOrder, delOrder];
