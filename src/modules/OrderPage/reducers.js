/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "OrderPage";

const initialState = freeze({
  updateOrderModal: false,
  delOrderModal: false,
  isDelOrderSuccess: false,
  isUpdate: false,
  isLoading: false,
  curOrder: {},
  listCategory: [],
  listOrders: [],
  pagination: {}
});

export default handleActions(
  {
    /*--
    ----Toggle Modal
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload.type === "updateOrderModal") {
        if (state.updateOrderModal) {
          return freeze({
            ...state,
            isUpdate: false,
            updateOrderModal: !state.updateOrderModal
          });
        }
        return freeze({
          ...state,
          updateOrderModal: !state.updateOrderModal
        });
      }
      if (action.payload.type === "delOrderModal") {
        return freeze({
          ...state,
          delOrderModal: !state.delOrderModal,
          curOrder: action.payload.data
        });
      }
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Cur Order
    */
    [actions.handleCurOrder]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: true,
        updateOrderModal: true,
        curOrder: action.payload
      });
    },
    /*--
    ---- Get All Order 
    */
    [actions.getAllOrders]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        isDelOrderSuccess: false
      });
    },
    [actions.getAllOrdersSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        listOrders: action.payload.data,
        pagination: action.payload.pagination
      });
    },
    [actions.getAllOrdersFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    /*--
    ---- Update Order
    */
    [actions.updateOrderSuccess]: (state, action) => {
      let tempt = [...state.listOrders];
      const index = tempt.findIndex(item => item._id === action.payload._id);
      tempt[index] = action.payload;
      return freeze({
        ...state,
        updateOrderModal: false,
        isUpdate: false,
        listOrders: tempt
      });
    },
    [actions.updateOrderFail]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: false
      });
    },
    /*--
    ---- Del Order
    */
    [actions.delOrder]: (state, action) => {
      return freeze({
        ...state,
        isDelOrderSuccess: false
      });
    },
    [actions.delOrderSuccess]: (state, action) => {
      let tempt = [...state.listOrders];
      const index = tempt.findIndex(
        item => item._id === action.payload.data._id
      );
      tempt.splice(index, 1);

      return freeze({
        ...state,
        listOrders: tempt,
        isDelOrderSuccess: true
      });
    },
    [actions.delOrderFail]: (state, action) => {
      return freeze({
        ...state,
        isDelOrderSuccess: false
      });
    }
  },
  initialState
);
