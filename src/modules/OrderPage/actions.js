/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

/* DISCOUNTS */

export const getAllOrders = createAction(CONST.GET_ALL_ORDERS);
export const getAllOrdersSuccess = createAction(CONST.GET_ALL_ORDERS_SUCCESS);
export const getAllOrdersFail = createAction(CONST.GET_ALL_ORDERS_FAIL);

export const updateOrder = createAction(CONST.UPDATE_ORDER);
export const updateOrderSuccess = createAction(CONST.UPDATE_ORDER_SUCCESS);
export const updateOrderFail = createAction(CONST.UPDATE_ORDER_FAIL);

export const delOrder = createAction(CONST.DEL_ORDER);
export const delOrderSuccess = createAction(CONST.DEL_ORDER_SUCCESS);
export const delOrderFail = createAction(CONST.DEL_ORDER_FAIL);

/* HANDLE */
export const toggleModal = createAction(CONST.TOGGLE_MODAL);
export const handleCurOrder = createAction(CONST.HANDLE_CUR_ORDER);
