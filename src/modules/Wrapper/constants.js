/**
 * @file constants
 */

/*---------------------------------------------------------------------*/
export const CHECK_LOGIN = "WRAPPER/CHECK_LOGIN";
export const CHECK_LOGIN_SUCCESS = "WRAPPER/CHECK_LOGIN_SUCCESS";
export const CHECK_LOGIN_FAIL = "WRAPPER/CHECK_LOGIN_FAIL";

export const CHANGE_PASSWORD = "WRAPPER/CHANGE_PASSWORD";
export const CHANGE_PASSWORD_SUCCESS = "WRAPPER/CHANGE_PASSWORD_SUCCESS";
export const CHANGE_PASSWORD_FAIL = "WRAPPER/CHANGE_PASSWORD_FAIL";

export const LOG_OUT = "WRAPPER/LOG_OUT";
export const TOGGLE_MODAL = "WRAPPER/TOGGLE_MODAL";