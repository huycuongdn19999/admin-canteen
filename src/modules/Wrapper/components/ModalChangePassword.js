import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import ChangePasswordForm from "./ChangePasswordForm";

const modalChangePassword = ({ open, toggleModal, props }) => {
  return (
    <Modal isOpen={open}>
      <ModalHeader toggle={toggleModal}>Thay đổi mật khẩu</ModalHeader>
      <ModalBody>
        <ChangePasswordForm {...props} />
      </ModalBody>
    </Modal>
  );
};

export default modalChangePassword;
