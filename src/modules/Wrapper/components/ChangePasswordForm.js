import React from "react";
import { FormGroup, FormFeedback, Label, Input } from "reactstrap";
import { useForm } from "react-hook-form";

export default function App(props) {
  const { register, errors, handleSubmit } = useForm();
  const onSubmit = (data, e) => {
    props.actions.changePassword(data);

    setTimeout(() => {
      e.target.reset();
    }, 500);
  };

  return (
    <div className="card">
      <div className="body">
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormGroup>
            <Label>Mật khẩu cũ</Label>
            <Input
              type="password"
              name="passwordOld"
              innerRef={register({
                required: "Vui lòng không để trống",
                maxLength: {
                  value: 20,
                  message: "Không được nhập quá 20 kí tự"
                }
              })}
              invalid={errors.passwordOld ? true : false}
              placeholder="Nhập mật khẩu cũ"
            />
            <FormFeedback>
              {errors.passwordOld && errors.passwordOld.message}
            </FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label>Mật khẩu mới</Label>
            <Input
              type="password"
              name="password"
              innerRef={register({
                required: "Vui lòng không để trống",
                maxLength: {
                  value: 20,
                  message: "Không được nhập quá 20 kí tự"
                }
              })}
              invalid={errors.password ? true : false}
              placeholder="Nhập mật khẩu mới"
            />
            <FormFeedback>
              {errors.password && errors.password.message}
            </FormFeedback>
          </FormGroup>
          <FormGroup className="mt-2 text-right">
            <button
              className="btn btn-raised btn-primary waves-effect"
              type="submit"
            >
              SUBMIT
            </button>
            &nbsp;
            <button
              className="btn btn-raised btn-default waves-effect"
              onClick={e => {
                e.preventDefault();
                props.actions.toggleModal({ type: "changePasswordModal" });
              }}
            >
              Đóng
            </button>
          </FormGroup>
        </form>
      </div>
    </div>
  );
}
