import React from "react";
import { Link } from "react-router-dom";
import { urlFormat } from "services/urlFormat";
import classnames from "classnames";

const leftSideBar = props => {
  return (
    <aside id="leftsidebar" className="sidebar">
      <div className="navbar-brand">
        <button className="btn-menu ls-toggle-btn" type="button">
          <i className="zmdi zmdi-menu"></i>
        </button>
        <Link to="/">
          <img
            src={urlFormat("assets/images/lovebreadLogo.png")}
            alt="love-bread"
            width="40"
          />
          &nbsp;
          <span className="text-logo">Lovebread</span>
        </Link>
      </div>
      <div className="menu">
        <ul className="list">
          <li>
            <div className="authen-container j-content-sp-between">
              <div
                className="authen-item"
                onClick={() =>
                  props.actions.toggleModal({ type: "changePasswordModal" })
                }
              >
                <i className="zmdi zmdi-settings zmdi-hc-spin"></i>&nbsp;
                <span>Mật khẩu</span>
              </div>

              <div
                className="authen-item"
                onClick={() => props.actions.logOut()}
              >
                <i className="zmdi zmdi-power"></i>&nbsp; <span>Thoát</span>
              </div>
            </div>
          </li>
          <li
            className={classnames({
              active: props.location.pathname === "/"
            })}
          >
            <Link to="/">
              <i className="zmdi zmdi-store"></i>
              <span>Sản phẩm</span>
            </Link>
          </li>
          <li
            className={classnames({
              active: props.location.pathname === "/blog"
            })}
          >
            <Link to="/blog">
              <i className="zmdi zmdi-blogger"></i>
              <span>Bài viết</span>
            </Link>
          </li>
          <li
            className={classnames({
              active: props.location.pathname === "/recruit"
            })}
          >
            <Link to="/recruit">
              <i className="zmdi zmdi-accounts-add"></i>
              <span>Tuyển dụng</span>
            </Link>
          </li>
          <li
            className={classnames({
              active: props.location.pathname === "/discount-code"
            })}
          >
            <Link to="/discount-code">
              <i className="zmdi zmdi-key"></i>
              <span>Mã giảm giá</span>
            </Link>
          </li>
          <li
            className={classnames({
              active: props.location.pathname === "/user"
            })}
          >
            <Link to="/user">
              <i className="zmdi zmdi-account"></i>
              <span>Khách hàng</span>
            </Link>
          </li>
          <li
            className={classnames({
              active: props.location.pathname === "/order"
            })}
          >
            <Link to="/order">
              <i className="zmdi zmdi-shopping-cart"></i>
              <span>Đơn hàng</span>
            </Link>
          </li>
          <li
            className={classnames({
              active: props.location.pathname === "/contact"
            })}
          >
            <Link to="/contact">
              <i className="zmdi zmdi-comment"></i>
              <span>Liên hệ</span>
            </Link>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default leftSideBar;
