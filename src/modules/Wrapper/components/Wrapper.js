import React, { Component } from "react";
import LeftSideBar from "./LeftSideBar";
import ModalChangePassword from "./ModalChangePassword";
import { NotificationContainer } from "react-notifications";
import "react-notifications/lib/notifications.css";
class Wrapper extends Component {
  render() {
    const { changePasswordModal } = this.props;
    return (
      <React.Fragment>
        <div className="overlay"></div>
        <LeftSideBar {...this.props} />
        <NotificationContainer />
        <section className="content">
          <div className="body_scroll">{this.props.children}</div>
        </section>
        <ModalChangePassword
          open={changePasswordModal}
          toggleModal={() =>
            this.props.actions.toggleModal({ type: "changePasswordModal" })
          }
          props={this.props}
        />
      </React.Fragment>
    );
  }
}

export default Wrapper;
