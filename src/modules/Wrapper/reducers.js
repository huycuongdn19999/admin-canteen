/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Wrapper";

const initialState = freeze({
  changePasswordModal: false
});

export default handleActions(
  {
    /*-- 
    ---- Toggle Modal
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload.type === "changePasswordModal") {
        return freeze({
          ...state,
          changePasswordModal: !state.changePasswordModal
        });
      } else {
        return freeze({
          ...state
        });
      }
    },
    /*--
    ---- Change Password
    */
    [actions.changePasswordSuccess]: (state, action) => {
      return freeze({
        ...state,
        changePasswordModal: false
      });
    }
  },
  initialState
);
