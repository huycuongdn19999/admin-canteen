import { put, call } from "redux-saga/effects";
import { push } from "react-router-redux";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";
import * as wrapperAPI from "api/wrapper";
import _ from "lodash";
import { get, clearAll } from "services/localStoredService";
import { NotificationManager } from "react-notifications";

export function* handleChangePassword(action) {
  try {
    let res = yield call(wrapperAPI.changePassword, action.payload);
    switch (res.status) {
      case 200:
        NotificationManager.success(
          "Thay đổi mật khẩu thành công",
          "Thông báo",
          2000
        );
        yield put(actions.changePasswordSuccess(res.data));
        break;
      case 401:
        NotificationManager.error("Mật khẩu cũ không đúng", "Thông báo", 2000);
        yield put(actions.changePasswordFail());
        break;
      default:
        break;
    }
  } catch (err) {
    NotificationManager.error(
      "Vui lòng kiểm tra lại dữ liệu hoặc đường truyền",
      "Lỗi",
      2000
    );
    yield put(actions.changePasswordFail(err));
  }
}

export function* handleCheckLogin(action) {
  try {
    let accessToken = get("access_token");
    if (_.isEmpty(accessToken)) {
      yield put(actions.checkLoginFail());
      yield put(push("/login"));
    } else {
      yield put(actions.checkLoginSuccess(accessToken));
    }
  } catch (err) {
    yield put(actions.checkLoginFail(err));
  }
}

export function* handleLogOut(action) {
  try {
    clearAll();
    yield put(push("/login"));
  } catch (err) {}
}

/*-----------------------------------------------------------------*/

export function* checkLogin() {
  yield takeAction(actions.checkLogin, handleCheckLogin);
}
export function* logOut() {
  yield takeAction(actions.logOut, handleLogOut);
}
export function* changePassword() {
  yield takeAction(actions.changePassword, handleChangePassword);
}
/*-----------------------------------------------------------------*/
export default [checkLogin, logOut, changePassword];
