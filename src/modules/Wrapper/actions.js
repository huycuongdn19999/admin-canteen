/**
 * @file All actions will be listed here
 */

import { createAction } from 'redux-actions';
import * as CONST from './constants';

/*---------------------------------------------------------------------*/
export const checkLogin = createAction(CONST.CHECK_LOGIN);
export const checkLoginSuccess = createAction(CONST.CHECK_LOGIN_SUCCESS);
export const checkLoginFail = createAction(CONST.CHECK_LOGIN_FAIL);

export const changePassword = createAction(CONST.CHANGE_PASSWORD);
export const changePasswordSuccess = createAction(CONST.CHANGE_PASSWORD_SUCCESS);
export const changePasswordFail = createAction(CONST.CHANGE_PASSWORD_FAIL);

export const logOut = createAction(CONST.LOG_OUT);
export const toggleModal = createAction(CONST.TOGGLE_MODAL);