import React, { Component } from "react";
import { Link } from "react-router-dom";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };
  }

  componentDidUpdate(prevState) {
    // const { active } = this.state;
    // var element = document.getElementById("leftsidebar");
    // if (prevState.active !== active) {
    //   if (active) {
    //     element.classList.add("open");
    //   } else {
    //     element.classList.remove("open");
    //   }
    // }
  }

  handleMobileMenu = () => {
    const { active } = this.state;
    this.setState({
      active: !active,
    });
  };
  render() {
    const { title, subTitle, active } = this.props;
    return (
      <div className="block-header__left">
        <h2>{title}</h2>
        <ul className="breadcrumb">
          <li className="breadcrumb-item">
            <Link to="/" className="text-custom-pinky">
              <i className="zmdi zmdi-store"></i>
            </Link>
          </li>
          <li className="breadcrumb-item">{subTitle}</li>
          <li className="breadcrumb-item active">{active}</li>
        </ul>
        <button
          className="btn btn-primary btn-icon mobile_menu"
          type="button"
          onClick={this.handleMobileMenu}
        >
          <i className="zmdi zmdi-sort-amount-desc"></i>
        </button>
      </div>
    );
  }
}

export default Header;
