import React, { Component } from "react";
import classnames from "classnames";

class Pagination extends Component {
  handlePageChange = page => {
    const { limitItemPerPage } = this.props;
    this.props.actions.handlePageChange(page);
    this.props.actions.getAllBlogs({ limit: limitItemPerPage, page });
  };

  handlePageNext = page => {
    const { maxPage, limitItemPerPage } = this.props;
    if (page <= maxPage) {
      this.props.actions.handlePageChange(page);
      this.props.actions.getAllBlogs({ limit: limitItemPerPage, page });
    }
  };

  handlePagePrev = page => {
    const { limitItemPerPage } = this.props;
    if (page >= 1) {
      this.props.actions.handlePageChange(page);
      this.props.actions.getAllBlogs({ limit: limitItemPerPage, page });
    }
  };

  render() {
    const { pagination, curPage } = this.props;
    return (
      <div className="card">
        <ul className="pagination pagination-primary">
          <li
            className="page-item"
            onClick={() => this.handlePagePrev(curPage - 1)}
          >
            <span className="page-link">TRANG TRƯỚC</span>
          </li>
          {pagination.map((item, index) => {
            return (
              <li
                key={index}
                className={classnames("page-item", {
                  active: item.page === curPage
                })}
                onClick={() => this.handlePageChange(item.page)}
              >
                <span className="page-link">{item.page}</span>
              </li>
            );
          })}

          <li
            className="page-item"
            onClick={() => this.handlePageNext(curPage + 1)}
          >
            <span className="page-link">TRANG SAU</span>
          </li>
        </ul>
      </div>
    );
  }
}

export default Pagination;
