import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const modalConfirmDel = ({ open, toggleModal, title, desc, actions }) => {
  return (
    <Modal isOpen={open} toggle={toggleModal} centered>
      <ModalHeader toggle={toggleModal}>{title}</ModalHeader>
      <ModalBody>{desc}</ModalBody>
      <ModalFooter>
        <Button color="danger" onClick={actions}>
          Xoá
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default modalConfirmDel;
