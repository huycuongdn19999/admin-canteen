import React from "react";
import { Link } from "react-router-dom";
import { urlFormat } from "services/urlFormat";
import { withRouter } from "react-router-dom";

const notFoundPage = () => (
  <div className="authentication">
    <div className="container">
      <div className="row">
        <div className="col-lg-4 col-sm-12">
          <form className="card auth_form">
            <div className="header">
              <img
                className="logo"
                src={urlFormat("assets/images/lovebreadLogo.png")}
                alt="love-bread-logo"
              />
              <h5>LỖI 404</h5>
              <span>Trang không tìm thấy</span>
            </div>
            <div className="body">
              <Link
                to="/"
                className="btn btn-primary btn-block waves-effect waves-light"
              >
                QUAY VỀ
              </Link>
            </div>
          </form>
          <div className="copyright text-center">
            &copy;
            {new Date().getFullYear()},
            <span>
              <a href="templatespoint.net">Templates Point</a>
            </span>
          </div>
        </div>
        <div className="col-lg-8 col-sm-12">
          <div className="card">
            <img src={urlFormat("assets/images/404.svg")} alt="404" />
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default withRouter(notFoundPage);
