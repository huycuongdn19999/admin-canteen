/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
// import * as actions from "./actions";
// import { get, clearAll } from "services/localStoredService";

export const name = "LoginPage";

const initialState = freeze({});

export default handleActions({}, initialState);
