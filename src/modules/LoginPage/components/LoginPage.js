import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { urlFormat } from "services/urlFormat";
import LoginForm from "./LoginForm";

class LoginPage extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="authentication">
          <Container>
            <Row>
              <Col lg="4" sm="12">
                <LoginForm {...this.props} />
                <div className="copyright text-center">
                  &copy;
                  <span>{new Date().getFullYear()}</span>
                  <span>
                    <a href="templatespoint.net">Templates Point</a>
                  </span>
                </div>
              </Col>
              <Col lg="8" sm="12">
                <div className="card">
                  <img
                    src={urlFormat("assets/images/signin.svg")}
                    alt="Sign In"
                  />
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </React.Fragment>
    );
  }
}
export default LoginPage;
