import React from "react";
import {
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormFeedback
} from "reactstrap";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { urlFormat } from "services/urlFormat";

export default function App(props) {
  const { register, errors, handleSubmit } = useForm();

  const onSubmit = (data, e) => {
    props.actions.login(data);
    setTimeout(() => {
      e.target.reset();
    }, 500);
  };

  return (
    <form className="card auth_form" onSubmit={handleSubmit(onSubmit)}>
      <div className="header">
        <img
          className="logo"
          src={urlFormat("assets/images/lovebreadLogo.png")}
          alt=""
        />
        <h5>Log in</h5>
      </div>
      <div className="body">
        <InputGroup className="mb-3">
          <Input
            type="text"
            className="form-control"
            placeholder="Username"
            name="email"
            innerRef={register({
              required: true
            })}
            invalid={errors.email ? true : false}
          />
          <InputGroupAddon addonType="append">
            <InputGroupText>
              <i className="zmdi zmdi-account-circle"></i>
            </InputGroupText>
          </InputGroupAddon>
          <FormFeedback>
            {errors.email && " Vui lòng không để trống"}
          </FormFeedback>
        </InputGroup>

        <InputGroup className="mb-3">
          <Input
            type="password"
            className="form-control"
            placeholder="Password"
            name="password"
            innerRef={register({
              required: true
            })}
            invalid={errors.password ? true : false}
          />
          <InputGroupAddon addonType="append">
            <InputGroupText>
              <Link
                to="/forgot-password"
                className="forgot"
                title="Forgot Password"
              >
                <i className="zmdi zmdi-lock"></i>
              </Link>
            </InputGroupText>
          </InputGroupAddon>
          <FormFeedback>
            {errors.password && " Vui lòng không để trống"}
          </FormFeedback>
        </InputGroup>
        <button
          type="submit"
          className="btn btn-primary btn-block waves-effect waves-light"
        >
          SIGN IN
        </button>
      </div>
    </form>
  );
}
