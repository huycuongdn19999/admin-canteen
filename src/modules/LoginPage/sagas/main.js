import { call, put } from "redux-saga/effects";
import { push } from "react-router-redux";
import * as actions from "../actions";
import * as authAPI from "api/auth";
import { takeAction } from "services/forkActionSagas";
import { save } from "services/localStoredService";
import { NotificationManager } from "react-notifications";

export function* handleLogin(action) {
  try {
    let res = yield call(authAPI.login, action.payload);
    if (res.status === 200) {
      const { access_token, role } = res.data;
      save("access_token", access_token);
      save("role", role);
      yield put(actions.loginSuccess(res.data));
      yield put(push("/"));
    } else {
      yield put(actions.loginFail(res.data.message));
      NotificationManager.error(
        "Vui lòng kiểm tra lại tên tài khoản hoặc mật khẩu",
        "Thông báo",
        2000
      );
    }
  } catch (err) {
    yield put(actions.loginFail(err));
  }
}

/*----------------------------------------------------------*/
export function* login() {
  yield takeAction(actions.login, handleLogin);
}

/*----------------------------------------------------------*/
export default [login];
