/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "ContactPage";

const initialState = freeze({
  delContactModal: false,
  isDelContactSuccess: false,
  isUpdate: false,
  isLoading: false,
  curContact: {},
  listContacts: [],
  pagination: {}
});

export default handleActions(
  {
    /*--
    ----Toggle Modal
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload.type === "delContactModal") {
        return freeze({
          ...state,
          delContactModal: !state.delContactModal,
          curContact: action.payload.data
        });
      }
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Cur Order
    */
    [actions.handleCurContact]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: true,
        updateOrderModal: true,
        curContact: action.payload
      });
    },
    /*--
    ---- Get All Contacts 
    */
    [actions.getAllContacts]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        isDelContactSuccess: false
      });
    },
    [actions.getAllContactsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        listContacts: action.payload.data,
        pagination: action.payload.pagination
      });
    },
    [actions.getAllContactsFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    /*--
    ---- Del Contact
    */
    [actions.delContact]: (state, action) => {
      return freeze({
        ...state,
        isDelContactSuccess: false
      });
    },
    [actions.delContactSuccess]: (state, action) => {
      return freeze({
        ...state,
        isDelContactSuccess: true
      });
    },
    [actions.delContactFail]: (state, action) => {
      return freeze({
        ...state,
        isDelContactSuccess: false
      });
    }
  },
  initialState
);
