/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

/* DISCOUNTS */

export const getAllContacts = createAction(CONST.GET_ALL_CONTACTS);
export const getAllContactsSuccess = createAction(CONST.GET_ALL_CONTACTS_SUCCESS);
export const getAllContactsFail = createAction(CONST.GET_ALL_CONTACTS_FAIL);

export const delContact = createAction(CONST.DEL_CONTACT);
export const delContactSuccess = createAction(CONST.DEL_CONTACT_SUCCESS);
export const delContactFail = createAction(CONST.DEL_CONTACT_FAIL);

/* HANDLE */
export const toggleModal = createAction(CONST.TOGGLE_MODAL);
export const handleCurContact = createAction(CONST.HANDLE_CUR_CONTACT);
