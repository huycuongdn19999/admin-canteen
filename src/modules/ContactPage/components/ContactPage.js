import React, { Component } from "react";
import ModalConfirmDel from "../../common/ModalConfirmDel";
import Header from "../../common/Header";
import ContactList from "./ContactList";
import { Container, Row } from "reactstrap";

class ContactPage extends Component {
  componentDidMount() {
    this.props.actions.getAllContacts({ limit: 10, page: 1 });
  }

  render() {
    const { curContact, delContactModal } = this.props;
    return (
      <React.Fragment>
        <div className="block-header">
          <div className="block-header__container">
            <Header
              title={"Danh sách đơn hàng"}
              subTitle={"Đơn hàng"}
              active={"Danh sách"}
            />
          </div>
        </div>
        <Container fluid={true}>
          <Row className="clearfix">
            <ContactList {...this.props} />
          </Row>
        </Container>
        <ModalConfirmDel
          open={delContactModal}
          toggleModal={() =>
            this.props.actions.toggleModal({
              type: "delContactModal"
            })
          }
          title={"Xác nhận xoá đơn hàng này"}
          desc="Bạn có chắc chắn xoá đơn hàng này"
          actions={() => {
            this.props.actions.delContact({ contactId: curContact._id });
            this.props.actions.toggleModal({
              type: "delContactModal"
            });
          }}
        />
      </React.Fragment>
    );
  }
}

export default ContactPage;
