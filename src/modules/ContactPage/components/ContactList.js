import React, { Component } from "react";
import LoadingScreen from "../../common/LoadingScreen";
import { Col } from "reactstrap";
import DataTable from "react-data-table-component";
import memoize from "memoize-one";

const customStyles = {
  headCells: {
    style: {
      backgroundColor: "#e47297",
      color: "#ffffff",
      border: "none",
      fontWeight: 600
    }
  }
};

const columns = memoize(props => [
  {
    name: "Họ và tên",
    selector: "name"
  },
  {
    name: "Email",
    selector: "email",
    grow: 2
  },
  {
    name: "Điện thoại",
    selector: "phone"
  },
  {
    name: "Công cụ",
    center: true,
    cell: (row, index) => (
      <div>
        <span
          className="btn btn-danger waves-effect waves-float btn-sm waves-pink"
          onClick={() => {
            props.actions.toggleModal({ type: "delContactModal", data: row });
          }}
        >
          <i className="zmdi zmdi-delete"></i>
        </span>
      </div>
    )
  }
]);

const ExpandedComponent = ({ data, actions }) => (
  <div className="expanded-container">
    <p>{data.message}</p>
  </div>
);

class ContactList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      limit: 10
    };
  }

  componentDidUpdate(prevProps) {
    const { limit, page } = this.state;
    const { isDelContactSuccess } = this.props;
    if (
      prevProps.isDelContactSuccess !== isDelContactSuccess &&
      isDelContactSuccess
    ) {
      this.props.actions.getAllContacts({
        page: Number.parseInt(page),
        limit
      });
    }
  }

  _handlePageChange = page => {
    const { limit } = this.state;
    this.props.actions.getAllContacts({
      page: Number.parseInt(page),
      limit
    });
    this.setState({
      page
    });
  };

  _handlePerRowsChange = row => {
    const { page } = this.state;
    this.props.actions.getAllContacts({
      page: Number.parseInt(page),
      limit: row
    });
  };

  render() {
    const { listContacts, pagination, isLoading } = this.props;
    return (
      <Col lg="12">
        <div className="card">
          <DataTable
            data={listContacts}
            columns={columns(this.props)}
            highlightOnHover
            noHeader={true}
            customStyles={customStyles}
            pagination
            paginationServer={true}
            paginationTotalRows={pagination.totalItem}
            onChangeRowsPerPage={this._handlePerRowsChange}
            onChangePage={this._handlePageChange}
            progressPending={isLoading}
            progressComponent={<LoadingScreen />}
            expandableRows
            expandOnRowClicked
            expandableRowsComponent={<ExpandedComponent />}
            noDataComponent="Hiện không có thông tin liên hệ nào"
          />
        </div>
      </Col>
    );
  }
}

export default ContactList;
