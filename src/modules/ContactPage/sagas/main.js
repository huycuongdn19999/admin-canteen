import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as contactsAPI from "api/contacts";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

/* CONTACTS */

export function* handleGetAllContacts(action) {
  try {
    let res = yield call(contactsAPI.getContactsPerPage, action.payload);
    yield put(actions.getAllContactsSuccess(res.data));
  } catch (err) {
    yield put(actions.getAllContactsFail(err));
  }
}

export function* handleDelContact(action) {
  try {
    let res = yield call(contactsAPI.delContact, action.payload);
    NotificationManager.success(
      "Xoá thông tin liên hệ thành công",
      "Thông báo",
      2000
    );
    yield put(actions.delContactSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.delContactFail(err));
  }
}

/*----------------------------------------------------------*/

/* CONTACTS */
export function* getAllContacts() {
  yield takeAction(actions.getAllContacts, handleGetAllContacts);
}
export function* delContact() {
  yield takeAction(actions.delContact, handleDelContact);
}
/*----------------------------------------------------------*/
export default [getAllContacts, delContact];
