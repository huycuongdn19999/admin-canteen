import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import AddProductForm from "./AddProductForm";

const modalAddProduct = ({ open, toggleModal, props }) => {
  return (
    <Modal isOpen={open} size="lg">
      <ModalHeader toggle={toggleModal}>Thông tin sản phẩm</ModalHeader>
      <ModalBody>
        <AddProductForm {...props} />
      </ModalBody>
    </Modal>
  );
};

export default modalAddProduct;
