import React, { Component } from "react";
import ModalAddProduct from "./ModalAddProduct";
import ModalConfirmDel from "../../common/ModalConfirmDel";
import Header from "../../common/Header";
import ProductList from "./ProductList";
import { Container, Row } from "reactstrap";

class ProductsPage extends Component {
  render() {
    const { curProduct, addProductModal, delProductModal } = this.props;
    return (
      <React.Fragment>
        <div className="block-header">
          <div className="block-header__container">
            <Header
              title={"Danh sách sản phẩm"}
              subTitle={"Sản phẩm"}
              active={"Danh sách"}
            />
            <div className="block-header__right">
              <button
                className="btn btn-success btn-icon"
                type="button"
                onClick={() =>
                  this.props.actions.toggleModal({ type: "addProductModal" })
                }
              >
                <i className="zmdi zmdi-plus"></i>
              </button>
            </div>
          </div>
        </div>
        <Container fluid={true}>
          <Row className="clearfix">
            <ProductList {...this.props} />
          </Row>
        </Container>
        <ModalAddProduct
          open={addProductModal}
          toggleModal={() =>
            this.props.actions.toggleModal({ type: "addProductModal" })
          }
          props={this.props}
        />
        <ModalConfirmDel
          open={delProductModal}
          toggleModal={() =>
            this.props.actions.toggleModal({
              type: "delProductModal"
            })
          }
          title={"Xác nhận xoá sản phẩm"}
          desc="Bạn có chắc chắn xoá sản phẩm này"
          actions={() => {
            this.props.actions.delProduct({ productId: curProduct._id });
            this.props.actions.toggleModal({
              type: "delProductModal"
            });
          }}
        />
      </React.Fragment>
    );
  }
}

export default ProductsPage;
