import React, { useEffect, useState } from "react";
import {
  FormGroup,
  FormFeedback,
  Label,
  Input,
  FormText,
  Alert
} from "reactstrap";
import imageCompression from "browser-image-compression";
import { useForm } from "react-hook-form";
export default function App(props) {
  const { register, errors, handleSubmit, setValue } = useForm();
  const { listCategory, isUpdate, curProduct } = props;

  const [compressStatus, setStatus] = useState(false);

  useEffect(() => {
    if (isUpdate) {
      setValue([
        { name_product: curProduct.name_product },
        { price: curProduct.price },
        { unit: curProduct.unit },
        { categoryId: curProduct.categoryId._id }
      ]);
    }
  }, [
    setValue,
    isUpdate,
    curProduct.name_product,
    curProduct.price,
    curProduct.unit,
    curProduct.categoryId
  ]);

  async function _handleImageCompress(imagesUpload) {
    var options = {
      maxSizeMB: 0.3,
      maxWidthOrHeight: 1920,
      onProgress: () => setStatus(true)
    };
    const imageFile = imagesUpload;
    const compressedFile = await imageCompression(imageFile, options);
    var newFile = new File([compressedFile], `thumbnail.png`);

    props.actions.handleFileChange(newFile);
    setStatus(false);
  }

  const onSubmit = (data, e) => {
    const { image } = props;
    if (isUpdate) {
      if (image !== "") {
        props.actions.updateProduct({
          ...data,
          image,
          productId: curProduct._id
        });
      } else {
        props.actions.updateProduct({
          ...data,
          productId: curProduct._id
        });
      }
    } else {
      props.actions.postProduct({ ...data, image });
    }
    setTimeout(() => {
      e.target.reset();
    }, 500);
  };

  return (
    <div className="card">
      <div className="body">
        <form onSubmit={handleSubmit(onSubmit)}>
          {compressStatus && (
            <Alert color="primary" className="text-center">
              <i className="zmdi zmdi-spinner zmdi-hc-spin"></i>
              &nbsp; Đang nén ảnh vui lòng đợi trong giây lát
            </Alert>
          )}
          <div className="product_grid__container">
            <FormGroup>
              <Label>Tên mặt hàng</Label>
              <Input
                type="text"
                name="name_product"
                innerRef={register({
                  required: "Vui lòng không để trống",
                  maxLength: {
                    value: 30,
                    message: "Không được nhập quá 50 kí tự"
                  }
                })}
                invalid={errors.name_product ? true : false}
                placeholder="Ví dụ: Bánh ngọt"
              />
              <FormFeedback>
                {errors.name_product && errors.name_product.message}
              </FormFeedback>
            </FormGroup>
            <FormGroup>
              <Label for="select">Loại sản phẩm</Label>
              <Input
                type="select"
                name="categoryId"
                id="select"
                innerRef={register({ required: "Vui lòng không để trống" })}
                invalid={errors.categoryId ? true : false}
              >
                {listCategory.map(item => {
                  return (
                    <option key={`item__${item._id}`} value={item._id}>
                      {item.catetogory_name}
                    </option>
                  );
                })}
              </Input>
              <FormFeedback>
                {errors.categoryId && errors.categoryId.message}
              </FormFeedback>
            </FormGroup>
            <FormGroup>
              <Label>Giá</Label>
              <Input
                type="text"
                name="price"
                innerRef={register({
                  required: " Vui lòng không để trống",
                  maxLength: {
                    value: 30,
                    message: "Không được nhập quá 30 kí tự"
                  },
                  pattern: /^\d+$/
                })}
                invalid={errors.price ? true : false}
                placeholder="Ví dụ: 30000"
              />
              <FormFeedback>
                {errors.price && errors.price.message}
              </FormFeedback>
            </FormGroup>
            <FormGroup>
              <Label>Đơn vị tính</Label>
              <Input
                type="text"
                name="unit"
                innerRef={register({
                  required: "Vui lòng không để trống",
                  maxLength: {
                    value: 20,
                    message: "Không được nhập quá 20 kí tự"
                  }
                })}
                invalid={errors.unit ? true : false}
                placeholder="Ví dụ: Cái, một tá"
              />
              <FormFeedback>{errors.unit && errors.unit.message}</FormFeedback>
            </FormGroup>
            <FormGroup>
              <Label for="imageInput">Hình ảnh</Label>
              <Input
                id="imageInput"
                name="image"
                type="file"
                accept="image/*"
                innerRef={isUpdate ? "" : register({ required: true })}
                invalid={errors.image ? true : false}
                onChange={e => {
                  setStatus(true);
                  _handleImageCompress(e.target.files[0]);
                }}
              />
              <FormFeedback>Vui lòng không để trống</FormFeedback>
              <FormText>
                Vui lòng chọn ảnh dưới 1MB và đợi nén ảnh hoàn tất trước khi
                thực hiện thao tác khác
              </FormText>
            </FormGroup>
            <FormGroup className="mt-2 text-right">
              <button
                className="btn btn-raised btn-primary waves-effect"
                type="submit"
              >
                SUBMIT
              </button>
              &nbsp;
              <button
                className="btn btn-raised btn-default waves-effect"
                onClick={e => {
                  e.preventDefault();
                  props.actions.toggleModal({ type: "addProductModal" });
                }}
              >
                Đóng
              </button>
            </FormGroup>
          </div>
        </form>
      </div>
    </div>
  );
}
