import React, { Component } from "react";
import LoadingScreen from "../../common/LoadingScreen";
import { Col } from "reactstrap";
import DataTable from "react-data-table-component";
import memoize from "memoize-one";
import { urlExFormat } from "services/urlFormat";

const customStyles = {
  headCells: {
    style: {
      backgroundColor: "#e47297",
      color: "#ffffff",
      border: "none",
      fontWeight: 600,
    },
  },
};

const columns = memoize((props) => [
  {
    name: "Thumbnail",
    selector: "image",
    cell: (row, index) => {
      return (
        <div style={{ width: "5rem" }} className="py-1 px-1">
          <img src={urlExFormat(row.image)} alt={row.name_product} />
        </div>
      );
    },
  },
  {
    name: "Tên sản phẩm",
    selector: "name_product",
    sortable: true,
  },
  {
    name: "Loại",
    selector: "catetogory_name",
    sortable: true,
  },
  {
    name: "Giá",
    selector: "price",
  },
  {
    name: "Đơn vị tính",
    selector: "unit",
  },
  {
    name: "Công cụ",
    center: true,
    cell: (row, index) => (
      <div>
        <span
          className="btn btn-primary waves-effect waves-float btn-sm waves-light-blue"
          onClick={() => {
            props.actions.handleCurProduct(row);
          }}
        >
          <i className="zmdi zmdi-edit"></i>
        </span>
        <span
          className="btn btn-danger waves-effect waves-float btn-sm waves-pink"
          onClick={() => {
            props.actions.toggleModal({ type: "delProductModal", data: row });
          }}
        >
          <i className="zmdi zmdi-delete"></i>
        </span>
      </div>
    ),
  },
]);
class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      limit: 10,
    };
  }

  componentDidUpdate(prevProps) {
    const { limit, page } = this.state;
    const { isDelProductSuccess, isAddProductSuccess } = this.props;
    if (
      (prevProps.isDelProductSuccess !== isDelProductSuccess &&
        isDelProductSuccess) ||
      (prevProps.isAddProductSuccess !== isAddProductSuccess &&
        isAddProductSuccess)
    ) {
      this.props.actions.getAllProducts({
        limit,
        page,
      });
    }
  }

  _handlePageChange = (page) => {
    const { limit } = this.state;
    this.props.actions.getAllProducts({
      page: Number.parseInt(page),
      limit,
    });
    this.setState({
      page,
    });
  };

  _handlePerRowsChange = (row) => {
    const { page } = this.state;
    this.props.actions.getAllProducts({
      page: Number.parseInt(page),
      limit: row,
    });
  };

  render() {
    // const { listProducts, pagination, isLoading } = this.props;
    const { isLoading } = this.props;
    console.log(this.props, "==this.props==");
    const pagination = {
      totalItem: 9999,
      itemPerPage: 9999,
    };
    const listProducts = [
      {
        image: "",
        name_product: "Ca chien",
        catetogory_name: "aaaa",
        price: 333,
        unit: "fasdf",
      },
    ];
    console.log(listProducts, "==listProducts==");
    return (
      <Col lg="12">
        <div className="card">
          <DataTable
            data={listProducts}
            columns={columns(this.props)}
            highlightOnHover
            noHeader={true}
            customStyles={customStyles}
            pagination
            paginationServer={true}
            paginationTotalRows={pagination.totalItem}
            onChangeRowsPerPage={this._handlePerRowsChange}
            onChangePage={this._handlePageChange}
            progressPending={isLoading}
            progressComponent={<LoadingScreen />}
            noDataComponent="Hiện không có mặt hàng nào"
          />
        </div>
      </Col>
    );
  }
}

export default ProductList;
