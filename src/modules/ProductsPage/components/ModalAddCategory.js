import React from "react";
import { FormGroup, FormFeedback, Label, Input, Row, Col } from "reactstrap";
import { useForm } from "react-hook-form";

export default function App(props) {
  const { register, errors, handleSubmit } = useForm();
  const onSubmit = (data, e) => {
    props.actions.postCategory(data);
    e.target.reset();
  };
  
  return (
    <div className="card">
      <div className="body">
        <form onSubmit={handleSubmit(onSubmit)}>
          <Row>
            <Col lg="6" md="12" sm="12">
              <FormGroup>
                <Label>Tên loại mặt hàng</Label>
                <Input
                  type="text"
                  name="catetogory_name"
                  innerRef={register({ required: true, maxLength: 50 })}
                  invalid={errors.catetogory_name ? true : false}
                  placeholder="Ví dụ: Bánh ngọt"
                />
                <FormFeedback>
                  {errors.catetogory_name &&
                  errors.catetogory_name.type === "required"
                    ? " Vui lòng không để trống"
                    : "Không được nhập quá 50 kí tự"}
                </FormFeedback>
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12">
              <FormGroup>
                <Label for="imageInput">Hình ảnh</Label>
                <Input
                  id="imageInput"
                  name="image"
                  type="file"
                  innerRef={register({ required: true })}
                  invalid={errors.image ? true : false}
                />
                <FormFeedback>Vui lòng không để trống</FormFeedback>
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12">
              <FormGroup>
                <Label>Mô tả</Label>
                <Input
                  type="textarea"
                  name="description"
                  innerRef={register({ required: true })}
                  invalid={errors.description ? true : false}
                  placeholder="Ví dụ: Bánh có nhiều đường..."
                />
                <FormFeedback>Vui lòng không để trống</FormFeedback>
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12" className="d-flex a-items-center">
              <button
                className="btn btn-raised btn-primary waves-effect"
                type="submit"
              >
                SUBMIT
              </button>
            </Col>
          </Row>
        </form>
      </div>
    </div>
  );
}
