/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const getAllCategory = createAction(CONST.GET_ALL_CATEGORY);
export const getAllCategorySuccess = createAction(
  CONST.GET_ALL_CATEGORY_SUCCESS
);
export const getAllCategoryFail = createAction(CONST.GET_ALL_CATEGORY_FAIL);

export const postCategory = createAction(CONST.POST_CATEGORY);
export const postCategorySuccess = createAction(CONST.POST_CATEGORY_SUCCESS);
export const postCategoryFail = createAction(CONST.POST_CATEGORY_FAIL);

export const updateCategory = createAction(CONST.UPDATE_CATEGORY);
export const updateCategorySuccess = createAction(
  CONST.UPDATE_CATEGORY_SUCCESS
);
export const updateCategoryFail = createAction(CONST.UPDATE_CATEGORY_FAIL);

export const delCategory = createAction(CONST.DEL_CATEGORY);
export const delCategorySuccess = createAction(CONST.DEL_CATEGORY_SUCCESS);
export const delCategoryFail = createAction(CONST.DEL_CATEGORY_FAIL);

/* PRODUCTS */

export const getAllProducts = createAction(CONST.GET_ALL_PRODUCTS);
export const getAllProductsSuccess = createAction(
  CONST.GET_ALL_PRODUCTS_SUCCESS
);
export const getAllProductsFail = createAction(CONST.GET_ALL_PRODUCTS_FAIL);

export const postProduct = createAction(CONST.POST_PRODUCT);
export const postProductSuccess = createAction(CONST.POST_PRODUCT_SUCCESS);
export const postProductFail = createAction(CONST.POST_PRODUCT_FAIL);

export const updateProduct = createAction(CONST.UPDATE_PRODUCT);
export const updateProductSuccess = createAction(CONST.UPDATE_PRODUCT_SUCCESS);
export const updateProductFail = createAction(CONST.UPDATE_PRODUCT_FAIL);

export const delProduct = createAction(CONST.DEL_PRODUCT);
export const delProductSuccess = createAction(CONST.DEL_PRODUCT_SUCCESS);
export const delProductFail = createAction(CONST.DEL_PRODUCT_FAIL);

/* Handle */

export const toggleModal = createAction(CONST.TOGGLE_MODAL);
export const handleCurProduct = createAction(CONST.HANDLE_CUR_PRODUCT);
export const handleFileChange = createAction(CONST.HANDLE_FILE_CHANGE);