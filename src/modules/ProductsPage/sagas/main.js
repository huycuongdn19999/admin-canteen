import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as categorysAPI from "api/categorys";
import * as productsAPI from "api/products";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

export function* handleGetAllCategory(action) {
  try {
    let res = yield call(categorysAPI.getCategorysPerPage);
    yield put(actions.getAllCategorySuccess(res.data));
  } catch (err) {
    yield put(actions.getAllCategoryFail(err));
  }
}

export function* handlePostCategory(action) {
  try {
    let res = yield call(categorysAPI.postCategory, action.payload);
    yield put(actions.postCategorySuccess(res.data));
  } catch (err) {
    yield put(actions.postCategoryFail(err));
  }
}

export function* handleDelCategory(action) {
  try {
    let res = yield call(categorysAPI.delCategory, action.payload);
    yield put(actions.delCategorySuccess(res.data));
  } catch (err) {
    yield put(actions.delCategoryFail(err));
  }
}

/* PRODUCTS */

export function* handleGetAllProducts(action) {
  try {
    let res = yield call(productsAPI.getProductsPerPage, action.payload);
    yield put(actions.getAllProductsSuccess(res.data));
  } catch (err) {
    yield put(actions.getAllProductsFail(err));
  }
}

export function* handlePostProduct(action) {
  try {
    let res = yield call(productsAPI.postProduct, action.payload);
    if (res.status === 200) {
      NotificationManager.success(
        "Thêm sản phẩm thành công",
        "Thông báo",
        2000
      );
      yield put(actions.postProductSuccess(res.data));
    } else {
      NotificationManager.error(
        "Vui lòng xem lại dữ liệu hoặc đường truyền!",
        "Lỗi",
        2000
      );
      yield put(actions.postProductFail(res.data));
    }
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.postProductFail(err));
  }
}

export function* handleUpdateProduct(action) {
  try {
    let res = yield call(productsAPI.updateProduct, action.payload);
    if (res.status === 200) {
      NotificationManager.success(
        "Cập nhật sản phẩm thành công",
        "Thông báo",
        2000
      );
      yield put(actions.updateProductSuccess(res.data));
    } else {
      NotificationManager.error(
        "Vui lòng xem lại dữ liệu hoặc đường truyền!",
        "Lỗi",
        2000
      );
      yield put(actions.updateProductFail(res.data));
    }
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.updateProductFail(err));
  }
}

export function* handleDelProduct(action) {
  try {
    let res = yield call(productsAPI.delProduct, action.payload);
    if (res.status === 200) {
      NotificationManager.success("Xoá sản phẩm thành công", "Thông báo", 2000);
      yield put(actions.delProductSuccess(res.data));
    } else {
      NotificationManager.error(
        "Vui lòng xem lại dữ liệu hoặc đường truyền!",
        "Lỗi",
        2000
      );
      yield put(actions.delProductFail(res.data));
    }
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.delProductFail(err));
  }
}

/*----------------------------------------------------------*/
export function* getAllCategory() {
  yield takeAction(actions.getAllCategory, handleGetAllCategory);
}
export function* postCategory() {
  yield takeAction(actions.postCategory, handlePostCategory);
}
export function* delCategory() {
  yield takeAction(actions.delCategory, handleDelCategory);
}

/* PRODUCTS */

export function* getAllProducts() {
  yield takeAction(actions.getAllProducts, handleGetAllProducts);
}
export function* postProduct() {
  yield takeAction(actions.postProduct, handlePostProduct);
}
export function* updateProduct() {
  yield takeAction(actions.updateProduct, handleUpdateProduct);
}
export function* delProduct() {
  yield takeAction(actions.delProduct, handleDelProduct);
}
/*----------------------------------------------------------*/
export default [
  getAllCategory,
  postCategory,
  delCategory,
  getAllProducts,
  postProduct,
  updateProduct,
  delProduct
];
