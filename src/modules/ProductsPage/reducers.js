/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "ProductsPage";

const initialState = freeze({
  addProductModal: false,
  delProductModal: false,
  isDelProductSuccess: false,
  isAddProductSuccess: false,
  isUpdate: false,
  isLoading: false,
  image: {},
  curProduct: {},
  listCategory: [],
  listProducts: [],
  pagination: {},
});

export default handleActions(
  {
    /*--
    ----Toggle Modal
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload.type === "addProductModal") {
        if (state.addProductModal) {
          return freeze({
            ...state,
            isUpdate: false,
            addProductModal: !state.addProductModal,
          });
        }
        return freeze({
          ...state,
          addProductModal: !state.addProductModal,
        });
      }
      if (action.payload.type === "delProductModal") {
        return freeze({
          ...state,
          delProductModal: !state.delProductModal,
          curProduct: action.payload.data,
        });
      }
      return freeze({
        ...state,
      });
    },
    /*--
    ----Handle Cur Product
    */
    [actions.handleCurProduct]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: true,
        addProductModal: true,
        curProduct: action.payload,
      });
    },
    /*--
    ----Handle File Change
    */
    [actions.handleFileChange]: (state, action) => {
      return freeze({
        ...state,
        image: action.payload,
      });
    },
    /*--
    ----Get All Category
    */
    [actions.getAllCategorySuccess]: (state, action) => {
      return freeze({
        ...state,
        listCategory: action.payload.data,
      });
    },
    /*--
    ---- Post Category 
    */
    [actions.postCategorySuccess]: (state, action) => {
      let tempt = [...state.listCategory];
      tempt.push(action.payload.data);
      return freeze({
        ...state,
        listCategory: tempt,
      });
    },
    /*--
    ---- Get All Products 
    */
    [actions.getAllProducts]: (state, aciton) => {
      return freeze({
        ...state,
        isLoading: true,
        isDelProductSuccess: false,
        isAddProductSuccess: false,
      });
    },
    [actions.getAllProductsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        listProducts: action.payload.data,
        pagination: action.payload.pagination,
      });
    },
    [actions.getAllProductsFail]: (state, aciton) => {
      return freeze({
        ...state,
        isLoading: false,
      });
    },
    /*--
    ---- Post Product
    */
    [actions.postProduct]: (state, action) => {
      return freeze({
        ...state,
        isAddProductSuccess: false,
      });
    },
    [actions.postProductSuccess]: (state, action) => {
      return freeze({
        ...state,
        addProductModal: false,
        isAddProductSuccess: true,
      });
    },
    [actions.postProductFail]: (state, action) => {
      return freeze({
        ...state,
        isAddProductSuccess: false,
      });
    },
    /*--
    ---- Update Product
    */
    [actions.updateProductSuccess]: (state, action) => {
      let tempt = [...state.listProducts];
      let temptCategory = [...state.listCategory];

      const index = tempt.findIndex((item) => item._id === action.payload._id);
      const indexCategory = temptCategory.findIndex(
        (item) => item._id === action.payload.categoryId
      );

      tempt[index] = {
        ...action.payload,
        categoryId: {
          _id: temptCategory[indexCategory]._id,
          catetogory_name: temptCategory[indexCategory].catetogory_name,
        },
      };
      return freeze({
        ...state,
        addProductModal: false,
        isUpdate: false,
        listProducts: tempt,
      });
    },
    [actions.updateProductFail]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: false,
      });
    },
    /*--
    ---- Del Product
    */
    [actions.delProduct]: (state, action) => {
      return freeze({
        ...state,
        isDelProductSuccess: false,
      });
    },
    [actions.delProductSuccess]: (state, action) => {
      let tempt = [...state.listProducts];
      const index = tempt.findIndex(
        (item) => item._id === action.payload.data._id
      );
      tempt.splice(index, 1);

      return freeze({
        ...state,
        listProducts: tempt,
        isDelProductSuccess: true,
      });
    },
    [actions.delProductFail]: (state, action) => {
      return freeze({
        ...state,
        isDelProductSuccess: false,
      });
    },
  },
  initialState
);
