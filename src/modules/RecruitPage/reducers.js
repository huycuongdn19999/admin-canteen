/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "RecruitPage";

const initialState = freeze({
  isUpdate: false,
  isLoading: false,
  isErrorContent: false,
  data: {}
});

export default handleActions(
  {
    /*--
    ----Handle Clear
    */
    [actions.handleClear]: (state, action) => {
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*--
    ----Handle File Change
    */
    [actions.handleFileChange]: (state, action) => {
      return freeze({
        ...state,
        isErrorThumb: false,
        data: {
          ...state.data,
          image: action.payload
        }
      });
    },
    /*--
    ----Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload === "content") {
        return freeze({
          ...state,
          isErrorContent: true,
          data: {
            ...state.data
          }
        });
      }

      return freeze({
        ...state,
        isErrorContent: true,
        data: {
          ...state.data
        }
      });
    },
    /*--
    ----Handle Content
    */
    [actions.handleContentEditor]: (state, action) => {
      return freeze({
        ...state,
        isErrorContent: false,
        data: {
          ...state.data,
          content: action.payload === "<p><br></p>" ? "" : action.payload
        }
      });
    },
    /*--
    ----Get Recruit Info
    */
    [actions.getRecruitInfo]: (state, action) => {
      return freeze({
        ...state
      });
    },
    [actions.getRecruitInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        data: action.payload
      });
    }
  },
  initialState
);
