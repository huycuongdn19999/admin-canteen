/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const getRecruitInfo = createAction(CONST.GET_RECRUIT_INFO);
export const getRecruitInfoSuccess = createAction(CONST.GET_RECRUIT_INFO_SUCCESS);
export const getRecruitInfoFail = createAction(CONST.GET_RECRUIT_INFO_FAIL);

export const updateRecruitInfo = createAction(CONST.UPDATE_RECRUIT_INFO);
export const updateRecruitInfoSuccess = createAction(CONST.UPDATE_RECRUIT_INFO_SUCCESS);
export const updateRecruitInfoFail = createAction(CONST.UPDATE_RECRUIT_INFO_FAIL);
/* Handle */
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleFileChange = createAction(CONST.HANDLE_FILE_CHANGE);

export const handleClear = createAction(CONST.HANDLE_CLEAR);
export const handleContentEditor = createAction(CONST.HANDLE_CONTENT_EDITOR);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);
