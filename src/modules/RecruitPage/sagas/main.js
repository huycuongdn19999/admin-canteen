import { call, put } from "redux-saga/effects";
import { takeAction } from "services/forkActionSagas";
import * as actions from "../actions";
import * as recruitAPI from "api/recruit";
import { NotificationManager } from "react-notifications";

export function* handleGetRecruitInfo(action) {
  try {
    let res = yield call(recruitAPI.getRecruitInfo);
    yield put(actions.getRecruitInfoSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.getRecruitInfoFail(err));
  }
}
export function* handleUpdateRecruitInfo(action) {
  try {
    let res = yield call(recruitAPI.updateRecruitInfo, action.payload);
    NotificationManager.success(
      "Cập nhật nội dung tuyển dụng thành công",
      "Thông báo",
      2000
    );
    yield put(actions.updateRecruitInfoSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.updateRecruitInfoFail(err));
  }
}
/*----------------------------------------------------------*/
export function* getRecruitInfo() {
  yield takeAction(actions.getRecruitInfo, handleGetRecruitInfo);
}
export function* updateRecruitInfo() {
  yield takeAction(actions.updateRecruitInfo, handleUpdateRecruitInfo);
}
/*----------------------------------------------------------*/
export default [getRecruitInfo, updateRecruitInfo];
