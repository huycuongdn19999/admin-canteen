import React, { Component } from "react";
import { FormGroup, FormFeedback, Label, Input, Col } from "reactstrap";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import _ from "lodash";

class RecruitEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputFile: ""
    };
  }

  modules = {
    toolbar: {
      container: [
        [{ header: "1" }, { header: "2" }, { font: [] }, { align: [] }],
        [{ size: [] }],
        ["bold", "italic", "underline", "strike", "blockquote"],
        [
          { list: "ordered" },
          { list: "bullet" },
          { indent: "-1" },
          { indent: "+1" }
        ]
      ]
    },
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false
    }
  };

  formats = [
    "header",
    "font",
    "align",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent"
  ];

  handleValidate = () => {
    const { content } = this.props.data;
    if (_.isEmpty(content)) {
      this.props.actions.handleValidate("content");
      return false;
    }
    return true;
  };

  handleUpdateRecruitInfo = () => {
    const { data } = this.props;
    this.setState({
      inputFile: ""
    });
    if (this.handleValidate()) {
      this.props.actions.updateRecruitInfo(data);
    }
  };

  handleEditorOnChange = html => {
    this.props.actions.handleContentEditor(html);
  };

  render() {
    const { isErrorContent } = this.props;
    const { content } = this.props.data;

    return (
      <React.Fragment>
        <Col lg="12">
          <div className="card">
            <div className="body">
              <FormGroup>
                <Label>Nội dung tuyển dụng</Label>
                <ReactQuill
                  onChange={this.handleEditorOnChange}
                  value={content || ""}
                  modules={this.modules}
                  formats={this.formats}
                />
                <Input type="hidden" invalid={isErrorContent} />
                <FormFeedback>Vui lòng không để trống</FormFeedback>
              </FormGroup>
              <button
                type="button"
                className="btn btn-info waves-effect"
                onClick={this.handleUpdateRecruitInfo}
              >
                SUBMIT
              </button>
            </div>
          </div>
        </Col>
      </React.Fragment>
    );
  }
}

export default RecruitEditor;
