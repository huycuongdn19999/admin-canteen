import React, { Component } from "react";
import { Container, Row } from "reactstrap";
import Header from "../../common/Header";
import RecruitEditor from "./RecruitEditor";

class RecruitPage extends Component {
  componentDidMount() {
    this.props.actions.getRecruitInfo();
  }
  render() {
    return (
      <React.Fragment>
        <div className="block-header">
          <div className="block-header__container">
            <Header
              title={"Thông tin tuyển dụng"}
              subTitle={"Tuyển dụng"}
              active={"Chỉnh sửa"}
            />
          </div>
        </div>

        <Container fluid={true}>
          <Row>
            <RecruitEditor {...this.props} />
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}

export default RecruitPage;
