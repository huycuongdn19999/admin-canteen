import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as discountsAPI from "api/discounts";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

/* DISCOUNT */

export function* handleGetAllDiscounts(action) {
  try {
    let res = yield call(discountsAPI.getDiscountsPerPage, action.payload);
    yield put(actions.getAllDiscountsSuccess(res.data));
  } catch (err) {
    yield put(actions.getAllDiscountsFail(err));
  }
}

export function* handlePostDiscount(action) {
  try {
    let res = yield call(discountsAPI.postDiscount, action.payload);
    NotificationManager.success(
      "Thêm mã giảm giá thành công",
      "Thông báo",
      2000
    );
    yield put(actions.postDiscountSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.postDiscountFail(err));
  }
}

export function* handleUpdateDiscount(action) {
  try {
    let res = yield call(discountsAPI.updateDiscount, action.payload);
    NotificationManager.success(
      "Cập nhật mã giảm giá thành công",
      "Thông báo",
      2000
    );
    yield put(actions.updateDiscountSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.updateDiscountFail(err));
  }
}

export function* handleDelDiscount(action) {
  try {
    let res = yield call(discountsAPI.delDiscount, action.payload);
    NotificationManager.success(
      "Xoá mã giảm giá thành công",
      "Thông báo",
      2000
    );
    yield put(actions.delDiscountSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.delDiscountFail(err));
  }
}

/*----------------------------------------------------------*/

/* DISCOUNTS */

export function* getAllDiscounts() {
  yield takeAction(actions.getAllDiscounts, handleGetAllDiscounts);
}
export function* postDiscount() {
  yield takeAction(actions.postDiscount, handlePostDiscount);
}
export function* updateDiscount() {
  yield takeAction(actions.updateDiscount, handleUpdateDiscount);
}
export function* delDiscount() {
  yield takeAction(actions.delDiscount, handleDelDiscount);
}
/*----------------------------------------------------------*/
export default [getAllDiscounts, postDiscount, updateDiscount, delDiscount];
