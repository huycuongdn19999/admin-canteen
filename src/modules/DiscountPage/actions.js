/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

/* DISCOUNTS */

export const getAllDiscounts = createAction(CONST.GET_ALL_DISCOUNTS);
export const getAllDiscountsSuccess = createAction(
  CONST.GET_ALL_DISCOUNTS_SUCCESS
);
export const getAllDiscountsFail = createAction(CONST.GET_ALL_DISCOUNTS_FAIL);

export const postDiscount = createAction(CONST.POST_DISCOUNT);
export const postDiscountSuccess = createAction(CONST.POST_DISCOUNT_SUCCESS);
export const postDiscountFail = createAction(CONST.POST_DISCOUNT_FAIL);

export const updateDiscount = createAction(CONST.UPDATE_DISCOUNT);
export const updateDiscountSuccess = createAction(CONST.UPDATE_DISCOUNT_SUCCESS);
export const updateDiscountFail = createAction(CONST.UPDATE_DISCOUNT_FAIL);

export const delDiscount = createAction(CONST.DEL_DISCOUNT);
export const delDiscountSuccess = createAction(CONST.DEL_DISCOUNT_SUCCESS);
export const delDiscountFail = createAction(CONST.DEL_DISCOUNT_FAIL);

/* Handle */

export const toggleModal = createAction(CONST.TOGGLE_MODAL);
export const handleCurDiscount = createAction(CONST.HANDLE_CUR_DISCOUNT);