/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "DiscountPage";

const initialState = freeze({
  addDiscountModal: false,
  delDiscountModal: false,
  isDelDiscountSuccess: false,
  isAddDiscountSuccess: false,
  isUpdate: false,
  isLoading: false,
  curDiscount: {},
  listCategory: [],
  listDiscounts: [],
  pagination: {}
});

export default handleActions(
  {
    /*--
    ----Toggle Modal
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload.type === "addDiscountModal") {
        if (state.addDiscountModal) {
          return freeze({
            ...state,
            isUpdate: false,
            addDiscountModal: !state.addDiscountModal
          });
        }
        return freeze({
          ...state,
          addDiscountModal: !state.addDiscountModal
        });
      }
      if (action.payload.type === "delDiscountModal") {
        return freeze({
          ...state,
          delDiscountModal: !state.delDiscountModal,
          curDiscount: action.payload.data
        });
      }
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Cur Discount
    */
    [actions.handleCurDiscount]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: true,
        addDiscountModal: true,
        curDiscount: action.payload
      });
    },
    /*--
    ---- Get All Discount 
    */
    [actions.getAllDiscounts]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        isDelDiscountSuccess: false,
        isAddDiscountSuccess: false
      });
    },
    [actions.getAllDiscountsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        listDiscounts: action.payload.data,
        pagination: action.payload.pagination
      });
    },
    [actions.getAllDiscountsFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    /*--
    ---- Post Discount
    */
    [actions.postDiscount]: (state, action) => {
      return freeze({
        ...state,
        isAddDiscountSuccess: false
      });
    },
    [actions.postDiscountSuccess]: (state, action) => {
      return freeze({
        ...state,
        addDiscountModal: false,
        isAddDiscountSuccess: true
      });
    },
    [actions.postDiscountFail]: (state, action) => {
      return freeze({
        ...state,
        isAddDiscountSuccess: false
      });
    },
    /*--
    ---- Update Discount
    */
    [actions.updateDiscountSuccess]: (state, action) => {
      let tempt = [...state.listDiscounts];
      const index = tempt.findIndex(item => item._id === action.payload._id);
      tempt[index] = action.payload;
      return freeze({
        ...state,
        addDiscountModal: false,
        isUpdate: false,
        listDiscounts: tempt
      });
    },
    [actions.updateDiscountFail]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: false
      });
    },
    /*--
    ---- Del Discount
    */
    [actions.delDiscount]: (state, action) => {
      return freeze({
        ...state,
        isDelDiscountSuccess: false
      });
    },
    [actions.delDiscountSuccess]: (state, action) => {
      let tempt = [...state.listDiscounts];
      const index = tempt.findIndex(item => item._id === action.payload._id);
      tempt.splice(index, 1);

      return freeze({
        ...state,
        listDiscounts: tempt,
        isDelDiscountSuccess: true
      });
    },
    [actions.delDiscountFail]: (state, action) => {
      return freeze({
        ...state,
        isDelDiscountSuccess: false
      });
    }
  },
  initialState
);
