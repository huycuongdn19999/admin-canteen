import React, { useEffect } from "react";
import { FormGroup, FormFeedback, Label, Input } from "reactstrap";
import { useForm } from "react-hook-form";

export default function App(props) {
  const { register, errors, handleSubmit, setValue } = useForm();
  const { isUpdate, curDiscount } = props;

  useEffect(() => {
    if (isUpdate) {
      setValue([
        { code: curDiscount.code },
        { discount: curDiscount.discount }
      ]);
    }
  }, [setValue, isUpdate, curDiscount.code, curDiscount.discount]);

  const onSubmit = (data, e) => {
    if (isUpdate) {
      props.actions.updateDiscount({
        data,
        discountId: curDiscount._id
      });
    } else {
      props.actions.postDiscount(data);
    }
    setTimeout(() => {
      e.target.reset();
    }, 500);
  };

  return (
    <div className="card">
      <div className="body">
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormGroup>
            <Label>Mã giảm giá</Label>
            <Input
              type="text"
              name="code"
              innerRef={register({
                required: "Vui lòng không để trống",
                maxLength: {
                  value: 20,
                  message: "Không được nhập quá 30 kí tự"
                }
              })}
              invalid={errors.code ? true : false}
              placeholder="Ví dụ: MUAHANG20"
              disabled={isUpdate}
            />
            <FormFeedback>{errors.code && errors.code.message}</FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label>Số tiền được giảm</Label>
            <Input
              type="text"
              name="discount"
              innerRef={register({
                required: "Vui lòng không để trống",
                maxLength: {
                  value: 20,
                  message: "Không được nhập quá 20 kí tự"
                },
                pattern: {
                  value: /^[0-9]*$/,
                  message: "Vui lòng nhập số"
                }
              })}
              invalid={errors.discount ? true : false}
              placeholder="Ví dụ: 30000"
            />
            <FormFeedback>
              {errors.discount && errors.discount.message}
            </FormFeedback>
          </FormGroup>
          <FormGroup className="mt-2 text-right">
            <button
              className="btn btn-raised btn-primary waves-effect"
              type="submit"
            >
              SUBMIT
            </button>
            &nbsp;
            <button
              className="btn btn-raised btn-default waves-effect"
              onClick={e => {
                e.preventDefault();
                props.actions.toggleModal({ type: "addDiscountModal" });
              }}
            >
              Đóng
            </button>
          </FormGroup>
        </form>
      </div>
    </div>
  );
}
