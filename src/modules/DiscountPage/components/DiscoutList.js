import React, { Component } from "react";
import LoadingScreen from "../../common/LoadingScreen";
import { Col } from "reactstrap";
import DataTable from "react-data-table-component";
import memoize from "memoize-one";

const customStyles = {
  headCells: {
    style: {
      backgroundColor: "#e47297",
      color: "#ffffff",
      border: "none",
      fontWeight: 600
    }
  }
};

const columns = memoize(props => [
  {
    name: "Mã",
    selector: "code"
  },
  {
    name: "Số tiền được giảm",
    selector: "discount",
    sortable: true
  },
  {
    name: "Công cụ",
    center: true,
    cell: (row, index) => (
      <div>
        <span
          className="btn btn-primary waves-effect waves-float btn-sm waves-light-blue"
          onClick={() => {
            props.actions.handleCurDiscount(row);
          }}
        >
          <i className="zmdi zmdi-edit"></i>
        </span>
        <span
          className="btn btn-danger waves-effect waves-float btn-sm waves-pink"
          onClick={() => {
            props.actions.toggleModal({ type: "delDiscountModal", data: row });
          }}
        >
          <i className="zmdi zmdi-delete"></i>
        </span>
      </div>
    )
  }
]);
class DiscoutList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      limit: 10
    };
  }

  componentDidUpdate(prevProps) {
    const { limit, page } = this.state;
    const { isDelDiscountSuccess, isAddDiscountSuccess } = this.props;
    if (
      (prevProps.isDelDiscountSuccess !== isDelDiscountSuccess &&
        isDelDiscountSuccess) ||
      (prevProps.isAddDiscountSuccess !== isAddDiscountSuccess &&
        isAddDiscountSuccess)
    ) {
      this.props.actions.getAllDiscounts({
        limit,
        page
      });
    }
  }

  _handlePageChange = page => {
    const { limit } = this.state;
    this.props.actions.getAllDiscounts({
      page: Number.parseInt(page),
      limit
    });
    this.setState({
      page
    });
  };

  _handlePerRowsChange = row => {
    const { page } = this.state;
    this.props.actions.getAllDiscounts({
      page: Number.parseInt(page),
      limit: row
    });
  };

  render() {
    const { listDiscounts, pagination, isLoading } = this.props;
    return (
      <Col lg="12">
        <div className="card">
          <DataTable
            data={listDiscounts}
            columns={columns(this.props)}
            highlightOnHover
            noHeader={true}
            customStyles={customStyles}
            pagination
            paginationServer={true}
            paginationTotalRows={pagination.totalItem}
            onChangeRowsPerPage={this._handlePerRowsChange}
            onChangePage={this._handlePageChange}
            progressPending={isLoading}
            progressComponent={<LoadingScreen />}
            noDataComponent="Hiện không có mã giảm giá nào"
          />
        </div>
      </Col>
    );
  }
}

export default DiscoutList;
