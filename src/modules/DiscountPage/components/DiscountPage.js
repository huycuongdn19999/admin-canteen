import React, { Component } from "react";
import ModalAddDiscount from "./ModalAddDiscount";
import ModalConfirmDel from "../../common/ModalConfirmDel";
import Header from "../../common/Header";
import DiscoutList from "./DiscoutList";
import { Container, Row } from "reactstrap";

class DiscountPage extends Component {
  componentDidMount() {
    this.props.actions.getAllDiscounts({ limit: 10, page: 1 });
  }
  render() {
    const { curDiscount, addDiscountModal, delDiscountModal } = this.props;
    return (
      <React.Fragment>
        <div className="block-header">
          <div className="block-header__container">
            <Header
              title={"Danh sách mã giảm giá"}
              subTitle={"Mã giảm giá"}
              active={"Danh sách"}
            />
            <div className="block-header__right">
              <button
                className="btn btn-success btn-icon"
                type="button"
                onClick={() =>
                  this.props.actions.toggleModal({ type: "addDiscountModal" })
                }
              >
                <i className="zmdi zmdi-plus"></i>
              </button>
            </div>
          </div>
        </div>
        <Container fluid={true}>
          <Row className="clearfix">
            <DiscoutList {...this.props} />
          </Row>
        </Container>
        <ModalAddDiscount
          open={addDiscountModal}
          toggleModal={() =>
            this.props.actions.toggleModal({ type: "addDiscountModal" })
          }
          props={this.props}
        />
        <ModalConfirmDel
          open={delDiscountModal}
          toggleModal={() =>
            this.props.actions.toggleModal({
              type: "delDiscountModal"
            })
          }
          title={"Xác nhận xoá mã giảm giá"}
          desc="Bạn có chắc chắn xoá mã giảm giá này"
          actions={() => {
            this.props.actions.delDiscount({ discountId: curDiscount._id });
            this.props.actions.toggleModal({
              type: "delDiscountModal"
            });
          }}
        />
      </React.Fragment>
    );
  }
}

export default DiscountPage;
