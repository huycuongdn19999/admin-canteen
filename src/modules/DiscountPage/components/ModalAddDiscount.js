import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import AddDiscountForm from "./AddDiscountForm";

const modalAddDiscount = ({ open, toggleModal, props }) => {
  return (
    <Modal isOpen={open}>
      <ModalHeader toggle={toggleModal}>Thông tin mã giảm giá</ModalHeader>
      <ModalBody>
        <AddDiscountForm {...props} />
      </ModalBody>
    </Modal>
  );
};

export default modalAddDiscount;
