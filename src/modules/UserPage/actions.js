/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

/* USERS */

export const getAllUsers = createAction(CONST.GET_ALL_USERS);
export const getAllUsersSuccess = createAction(CONST.GET_ALL_USERS_SUCCESS);
export const getAllUsersFail = createAction(CONST.GET_ALL_USERS_FAIL);

export const updateUser = createAction(CONST.UPDATE_USER);
export const updateUserSuccess = createAction(CONST.UPDATE_USER_SUCCESS);
export const updateUserFail = createAction(CONST.UPDATE_USER_FAIL);

export const delUser = createAction(CONST.DEL_USER);
export const delUserSuccess = createAction(CONST.DEL_USER_SUCCESS);
export const delUserFail = createAction(CONST.DEL_USER_FAIL);

/* HANDLE */
export const toggleModal = createAction(CONST.TOGGLE_MODAL);
export const handleCurUser = createAction(CONST.HANDLE_CUR_USER);
