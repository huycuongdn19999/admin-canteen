/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "UserPage";

const initialState = freeze({
  updateUserModal: false,
  delUserModal: false,
  isDelUserSuccess: false,
  isUpdate: false,
  isLoading: false,
  curUser: {},
  listUsers: [],
  pagination: {}
});

export default handleActions(
  {
    /*--
    ----Toggle Modal
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload.type === "updateUserModal") {
        if (state.updateUserModal) {
          return freeze({
            ...state,
            isUpdate: false,
            updateUserModal: !state.updateUserModal
          });
        }
        return freeze({
          ...state,
          updateUserModal: !state.updateUserModal
        });
      }
      if (action.payload.type === "delUserModal") {
        return freeze({
          ...state,
          delUserModal: !state.delUserModal,
          curUser: action.payload.data
        });
      }
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Cur User
    */
    [actions.handleCurUser]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: true,
        updateUserModal: true,
        curUser: action.payload
      });
    },
    /*--
    ---- Get All User 
    */
    [actions.getAllUsers]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true,
        isDelUserSuccess: false
      });
    },
    [actions.getAllUsersSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        listUsers: action.payload.data,
        pagination: action.payload.pagination
      });
    },
    [actions.getAllUsersFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    /*--
    ---- Update User
    */
    [actions.updateUserSuccess]: (state, action) => {
      let tempt = [...state.listUsers];
      const index = tempt.findIndex(item => item._id === action.payload._id);
      tempt[index] = action.payload;
      return freeze({
        ...state,
        updateUserModal: false,
        isUpdate: false,
        listUsers: tempt
      });
    },
    [actions.updateUserFail]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: false
      });
    },
    /*--
    ---- Del User
    */
    [actions.delUser]: (state, action) => {
      return freeze({
        ...state,
        isDelUserSuccess: false
      });
    },
    [actions.delUserSuccess]: (state, action) => {
      let tempt = [...state.listUsers];
      const index = tempt.findIndex(item => item._id === action.payload._id);
      tempt.splice(index, 1);

      return freeze({
        ...state,
        listUsers: tempt,
        isDelUserSuccess: true
      });
    },
    [actions.delUserFail]: (state, action) => {
      return freeze({
        ...state,
        isDelUserSuccess: false
      });
    }
  },
  initialState
);
