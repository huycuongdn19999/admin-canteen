import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as usersAPI from "api/users";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

/* USERS */

export function* handleGetAllUsers(action) {
  try {
    let res = yield call(usersAPI.getUsersPerPage, action.payload);
    yield put(actions.getAllUsersSuccess(res.data));
  } catch (err) {
    yield put(actions.getAllUsersFail(err));
  }
}

export function* handleUpdateUser(action) {
  try {
    let res = yield call(usersAPI.updateUser, action.payload);
    if (res.status === 200) {
      NotificationManager.success(
        "Cập nhật mật khẩu cho khách hàng thành công",
        "Thông báo",
        2000
      );
      yield put(actions.updateUserSuccess(res.data));
    } else {
      NotificationManager.error(
        "Cập nhật thất bại, vui lòng xem lại dữ liệu hoặc đường truyền!",
        "Thông báo",
        2000
      );
      yield put(actions.updateUserFail(res));
    }
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.updateUserFail(err));
  }
}

export function* handleDelUser(action) {
  try {
    let res = yield call(usersAPI.delUser, action.payload);
    if (res.status === 200) {
      NotificationManager.success(
        "Xoá khách hàng thành công",
        "Thông báo",
        2000
      );
      yield put(actions.delUserSuccess(res.data));
    } else {
      NotificationManager.error(
        "Xoá thất bại, vui lòng xem lại dữ liệu hoặc đường truyền!",
        "Lỗi",
        2000
      );
      yield put(actions.delUserFail(res.data));
    }
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.delUserFail(err));
  }
}

/*----------------------------------------------------------*/

export function* getAllUsers() {
  yield takeAction(actions.getAllUsers, handleGetAllUsers);
}
export function* updateUser() {
  yield takeAction(actions.updateUser, handleUpdateUser);
}
export function* delUser() {
  yield takeAction(actions.delUser, handleDelUser);
}
/*----------------------------------------------------------*/
export default [getAllUsers, updateUser, delUser];
