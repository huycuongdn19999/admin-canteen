import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import UpdateUserForm from "./UpdateUserForm";

const modalUpdateUser = ({ open, toggleModal, props }) => {
  return (
    <Modal isOpen={open}>
      <ModalHeader toggle={toggleModal}>Thông tin khách hàng</ModalHeader>
      <ModalBody>
        <UpdateUserForm {...props} />
      </ModalBody>
    </Modal>
  );
};

export default modalUpdateUser;
