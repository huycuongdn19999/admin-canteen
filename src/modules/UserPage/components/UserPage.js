import React, { Component } from "react";
import ModalUpdateUser from "./ModalUpdateUser";
import ModalConfirmDel from "../../common/ModalConfirmDel";
import Header from "../../common/Header";
import UserList from "./UserList";
import { Container, Row } from "reactstrap";

class UserPage extends Component {
  render() {
    const { curUser, updateUserModal, delUserModal } = this.props;
    return (
      <React.Fragment>
        <div className="block-header">
          <div className="block-header__container">
            <Header
              title={"Danh sách khách hàng"}
              subTitle={"Khách hàng"}
              active={"Danh sách"}
            />
          </div>
        </div>
        <Container fluid={true}>
          <Row className="clearfix">
            <UserList {...this.props} />
          </Row>
        </Container>
        <ModalUpdateUser
          open={updateUserModal}
          toggleModal={() =>
            this.props.actions.toggleModal({ type: "updateUserModal" })
          }
          props={this.props}
        />
        <ModalConfirmDel
          open={delUserModal}
          toggleModal={() =>
            this.props.actions.toggleModal({
              type: "delUserModal"
            })
          }
          title={"Xác nhận xoá khách hàng"}
          desc="Bạn có chắc chắn xoá khách hàng này"
          actions={() => {
            this.props.actions.delUser({ userId: curUser._id });
            this.props.actions.toggleModal({
              type: "delUserModal"
            });
          }}
        />
      </React.Fragment>
    );
  }
}

export default UserPage;
