import React, { useEffect } from "react";
import { FormGroup, FormFeedback, Label, Input } from "reactstrap";
import { useForm } from "react-hook-form";

export default function App(props) {
  const { register, errors, handleSubmit, setValue } = useForm();
  const { isUpdate, curUser } = props;

  useEffect(() => {
    if (isUpdate) {
      setValue([
        { email: curUser.email },
        { fullName: curUser.fullName },
        { phone: curUser.phone }
      ]);
    }
  }, [setValue, isUpdate, curUser.email, curUser.fullName, curUser.phone]);

  const onSubmit = (data, e) => {
    if (isUpdate) {
      props.actions.updateUser({
        data,
        userId: curUser._id
      });
    }
    setTimeout(() => {
      e.target.reset();
    }, 500);
  };

  return (
    <div className="card">
      <div className="body">
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormGroup>
            <Label>Họ và Tên</Label>
            <Input
              type="text"
              name="fullName"
              innerRef={register()}
              placeholder="Ví dụ: Nguyễn Văn A"
              disabled={isUpdate}
            />
          </FormGroup>
          <FormGroup>
            <Label>Email</Label>
            <Input
              type="text"
              name="email"
              innerRef={register}
              disabled={isUpdate}
            />
          </FormGroup>
          <FormGroup>
            <Label>Điện thoại</Label>
            <Input
              type="text"
              name="phone"
              innerRef={register}
              disabled={isUpdate}
            />
          </FormGroup>
          <FormGroup>
            <Label>Mật khẩu mới</Label>
            <Input
              type="password"
              name="password"
              innerRef={register({
                required: "Vui lòng không để trống",
                maxLength: {
                  value: 20,
                  message: "Không được nhập quá 20 kí tự"
                }
              })}
              placeholder="Nhập mật khẩu mới"
              invalid={errors.password ? true : false}
            />
            <FormFeedback>
              {errors.password && errors.password.message}
            </FormFeedback>
          </FormGroup>
          <FormGroup className="mt-2 text-right">
            <button
              className="btn btn-raised btn-primary waves-effect"
              type="submit"
            >
              SUBMIT
            </button>
            &nbsp;
            <button
              className="btn btn-raised btn-default waves-effect"
              onClick={e => {
                e.preventDefault();
                props.actions.toggleModal({ type: "updateUserModal" });
              }}
            >
              Đóng
            </button>
          </FormGroup>
        </form>
      </div>
    </div>
  );
}
