import React, { Component } from "react";
import LoadingScreen from "../../common/LoadingScreen";
import { Col } from "reactstrap";
import DataTable from "react-data-table-component";
import memoize from "memoize-one";

const customStyles = {
  headCells: {
    style: {
      backgroundColor: "#e47297",
      color: "#ffffff",
      border: "none",
      fontWeight: 600
    }
  }
};

const columns = memoize(props => [
  {
    name: "Họ và Tên",
    selector: "fullName",
    sortable: true
  },
  {
    name: "Email",
    selector: "email",
    sortable: true
  },
  {
    name: "Điện thoại",
    selector: "phone"
  },
  {
    name: "Công cụ",
    center: true,
    cell: (row, index) => (
      <div>
        <span
          className="btn btn-primary waves-effect waves-float btn-sm waves-light-blue"
          onClick={() => {
            props.actions.handleCurUser(row);
          }}
        >
          <i className="zmdi zmdi-edit"></i>
        </span>
        <span
          className="btn btn-danger waves-effect waves-float btn-sm waves-pink"
          onClick={() => {
            props.actions.toggleModal({ type: "delUserModal", data: row });
          }}
        >
          <i className="zmdi zmdi-delete"></i>
        </span>
      </div>
    )
  }
]);
class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      limit: 10
    };
  }

  componentDidUpdate(prevProps) {
    const { limit, page } = this.state;
    const { isDelUserSuccess } = this.props;
    if (prevProps.isDelUserSuccess !== isDelUserSuccess && isDelUserSuccess) {
      this.props.actions.getAllUsers({
        limit,
        page
      });
    }
  }

  _handlePageChange = page => {
    const { limit } = this.state;
    this.props.actions.getAllUsers({
      page: Number.parseInt(page),
      limit
    });
    this.setState({
      page
    });
  };

  _handlePerRowsChange = row => {
    const { page } = this.state;
    this.props.actions.getAllUsers({
      page: Number.parseInt(page),
      limit: row
    });
  };

  render() {
    const { listUsers, pagination, isLoading } = this.props;
    return (
      <Col lg="12">
        <div className="card">
          <DataTable
            data={listUsers}
            columns={columns(this.props)}
            highlightOnHover
            noHeader={true}
            customStyles={customStyles}
            pagination
            paginationServer={true}
            paginationTotalRows={pagination.totalItem}
            onChangeRowsPerPage={this._handlePerRowsChange}
            onChangePage={this._handlePageChange}
            progressPending={isLoading}
            progressComponent={<LoadingScreen />}
          />
        </div>
      </Col>
    );
  }
}

export default UserList;
