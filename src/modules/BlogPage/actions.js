/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const getAllBlogs = createAction(CONST.GET_ALL_BLOGS);
export const getAllBlogsSuccess = createAction(CONST.GET_ALL_BLOGS_SUCCESS);
export const getAllBlogsFail = createAction(CONST.GET_ALL_BLOGS_FAIL);

export const postBlog = createAction(CONST.POST_BLOG);
export const postBlogSuccess = createAction(CONST.POST_BLOG_SUCCESS);
export const postBlogFail = createAction(CONST.POST_BLOG_FAIL);

export const updateBlog = createAction(CONST.UPDATE_BLOG);
export const updateBlogSuccess = createAction(CONST.UPDATE_BLOG_SUCCESS);
export const updateBlogFail = createAction(CONST.UPDATE_BLOG_FAIL);

export const delBlog = createAction(CONST.DEL_BLOG);
export const delBlogSuccess = createAction(CONST.DEL_BLOG_SUCCESS);
export const delBlogFail = createAction(CONST.DEL_BLOG_FAIL);

/* Handle */
export const toggleModal = createAction(CONST.TOGGLE_MODAL);


/* Handle */
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleFileChange = createAction(CONST.HANDLE_FILE_CHANGE);
export const handleTabChange = createAction(CONST.HANDLE_TAB_CHANGE);
export const handlePageChange = createAction(CONST.HANDLE_PAGE_CHANGE);

export const handleCurBlog = createAction(CONST.HANDLE_CUR_BLOG);
export const handleClear = createAction(CONST.HANDLE_CLEAR);
export const handleContentEditor = createAction(CONST.HANDLE_CONTENT_EDITOR);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);
