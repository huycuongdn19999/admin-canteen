import { call, put } from "redux-saga/effects";
import { takeAction } from "services/forkActionSagas";
import * as actions from "../actions";
import * as blogAPI from "api/blogs";
import { NotificationManager } from "react-notifications";

export function* handleGetAllBlogs(action) {
  try {
    let res = yield call(blogAPI.getBlogsPerPage, action.payload);
    yield put(actions.getAllBlogsSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.getAllBlogsFail(err));
  }
}

export function* handlePostBlog(action) {
  try {
    let res = yield call(blogAPI.postBlog, action.payload);
    NotificationManager.success(
      "Thêm bài viết thành công",
      "Thông báo",
      2000
    );
    yield put(actions.postBlogSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.postBlogFail(err));
  }
}

export function* handleUpdateBlog(action) {
  try {
    let res = yield call(blogAPI.updateBlog, action.payload);
    NotificationManager.success(
      "Cập nhật bài viết thành công",
      "Thông báo",
      2000
    );
    yield put(actions.updateBlogSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.updateBlogFail(err));
  }
}

export function* handleDelBlog(action) {
  try {
    let res = yield call(blogAPI.delBlog, action.payload);
    NotificationManager.success("Xoá bài viết thành công", "Thông báo", 2000);
    yield put(actions.delBlogSuccess(res.data));
  } catch (err) {
    NotificationManager.error(
      "Vui lòng xem lại dữ liệu hoặc đường truyền!",
      "Lỗi",
      2000
    );
    yield put(actions.delBlogFail(err));
  }
}

/*----------------------------------------------------------*/
export function* getAllBlogs() {
  yield takeAction(actions.getAllBlogs, handleGetAllBlogs);
}
export function* postBlog() {
  yield takeAction(actions.postBlog, handlePostBlog);
}
export function* updateBlog() {
  yield takeAction(actions.updateBlog, handleUpdateBlog);
}
export function* delBlog() {
  yield takeAction(actions.delBlog, handleDelBlog);
}
/*----------------------------------------------------------*/
export default [getAllBlogs, postBlog, updateBlog, delBlog];
