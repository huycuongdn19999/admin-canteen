/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "BlogPage";

const initialState = freeze({
  delBlogModal: false,
  cancelBlogEdit: false,
  activedTab: "1",
  isUpdate: false,
  isLoading: false,
  data: {},
  curBlog: {},
  isErrorTitle: false,
  isErrorThumb: false,
  isErrorContent: false,
  listBlogs: [],

  pagination: [],
  limitPageShow: 3,
  limitItemPerPage: 6,
  curPage: 1,
  maxPage: 0
});

export default handleActions(
  {
    /*--
    ----Toggle Modal
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload.type === "delBlogModal") {
        return freeze({
          ...state,
          delBlogModal: !state.delBlogModal,
          curBlog: action.payload.data
        });
      }
      if (action.payload.type === "cancelBlogEdit") {
        return freeze({
          ...state,
          cancelBlogEdit: !state.cancelBlogEdit
        });
      }
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Cur Blog
    */
    [actions.handlePageChange]: (state, action) => {
      return freeze({
        ...state,
        curPage: action.payload
      });
    },
    /*--
    ----Handle Cur Blog
    */
    [actions.handleCurBlog]: (state, action) => {
      if (action.payload.type === "updateBlog") {
        return freeze({
          ...state,
          isUpdate: true,
          curBlog: action.payload.data,
          data: action.payload.data,
          activedTab: "1"
        });
      }
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Clear
    */
    [actions.handleClear]: (state, action) => {
      if (action.payload.type === "POST") {
        return freeze({
          ...state,
          data: {
            image: {},
            title: "",
            content: ""
          },
          isErrorTitle: false,
          isErrorThumb: false,
          isErrorContent: false
        });
      }
      return freeze({
        ...state
      });
    },
    /*--
    ----Handle Input Change
    */
    [actions.handleTabChange]: (state, action) => {
      if (action.payload.type === "2") {
        return freeze({
          ...state,
          data: {
            title: "",
            image: "",
            content: "",
            isErrorTitle: false,
            isErrorThumb: false,
            isErrorContent: false
          },
          activedTab: action.payload.type,
          isUpdate: false
        });
      } else {
        return freeze({
          ...state,
          activedTab: action.payload.type
        });
      }
    },
    /*--
    ----Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;

      if (name === "title") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorTitle: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorTitle: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*--
    ----Handle File Change
    */
    [actions.handleFileChange]: (state, action) => {
      return freeze({
        ...state,
        isErrorThumb: false,
        data: {
          ...state.data,
          image: action.payload
        }
      });
    },
    /*--
    ----Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload === "title") {
        return freeze({
          ...state,
          isErrorTitle: true,
          data: {
            ...state.data
          }
        });
      }

      if (action.payload === "image") {
        return freeze({
          ...state,
          isErrorThumb: true,
          data: {
            ...state.data
          }
        });
      }

      if (action.payload === "content") {
        return freeze({
          ...state,
          isErrorContent: true,
          data: {
            ...state.data
          }
        });
      }

      return freeze({
        ...state,
        isErrorTitle: true,
        isErrorThumb: true,
        isErrorContent: true,
        data: {
          ...state.data
        }
      });
    },
    /*--
    ----Handle Content
    */
    [actions.handleContentEditor]: (state, action) => {
      return freeze({
        ...state,
        isErrorContent: false,
        data: {
          ...state.data,
          content: action.payload === "<p><br></p>" ? "" : action.payload
        }
      });
    },
    /*--
    ----Get All Blogs
    */
    [actions.getAllBlogs]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.getAllBlogsSuccess]: (state, action) => {
      const { itemPerPage, totalItem } = action.payload.pagination;
      let maxPage = Math.ceil(totalItem / itemPerPage);
      let temptPagination = [];
      let pageStart = 1;
      let pageEnd = maxPage;
      if (maxPage > 0) {
        if (maxPage < state.limitPageShow) {
          for (let i = pageStart; i <= pageEnd; i++) {
            temptPagination.push({
              page: i
            });
          }
        } else {
          if (state.curPage === maxPage) {
            pageStart = maxPage - state.limitPageShow + 1;
          }
          if (state.curPage < maxPage && state.curPage > 1) {
            pageStart = state.curPage - 1;
            pageEnd = state.curPage + 1;
          }
          if (state.curPage === pageStart) {
            pageEnd = state.limitPageShow;
          }
          for (let i = pageStart; i <= pageEnd; i++) {
            temptPagination.push({
              page: i
            });
          }
        }
      }
      return freeze({
        ...state,
        isLoading: false,
        listBlogs: action.payload.data,
        pagination: temptPagination,
        maxPage
      });
    },
    [actions.getAllBlogsFail]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false
      });
    },
    /*--
    ----Update Blogs
    */
    [actions.updateBlogSuccess]: (state, action) => {
      return freeze({
        ...state,
        isUpdate: false,
        data: {
          title: "",
          content: ""
        }
      });
    },
    /*--
    ----Del Blog
    */
    [actions.delBlogSuccess]: (state, action) => {
      const { itemPerPage, totalItem } = action.payload.pagination;
      let maxPage = Math.ceil(totalItem / itemPerPage);

      let temptPagination = [];
      if (maxPage > 0) {
        if (maxPage < state.limitPageShow) {
          for (let i = state.curPage; i <= maxPage; i++) {
            temptPagination.push({
              page: i
            });
          }
        } else if (maxPage >= state.limitPageShow) {
          for (let i = state.curPage - 1; i <= maxPage; i++) {
            temptPagination.push({
              page: i
            });
          }
        }
      }

      let temptListBlogs = [...state.listBlogs];
      const index = temptListBlogs.findIndex(
        item => item._id === action.payload.data._id
      );
      temptListBlogs.splice(index, 1);

      return freeze({
        ...state,
        listBlogs: temptListBlogs,
        pagination: temptPagination,
        maxPage
      });
    }
  },
  initialState
);
