import React from "react";
import { urlExFormat } from "services/urlFormat";
import moment from "moment";
require("moment/locale/vi");

const stripHtml = html => {
  var tmp = document.createElement("p");
  tmp.innerHTML = html;
  var des = tmp.innerText;
  var shortDes = des.substring(0, 150);
  tmp.remove();
  return shortDes + "...";
};

const BlogItem = ({ blog, props }) => {
  return (
    <div className="blogitem mb-5">
      <div className="blogitem-image">
        <span className="blog-image">
          <img src={urlExFormat(blog.image)} alt={blog.title} />
        </span>
        <span className="blogitem-date">
          {moment(blog.createdAt).format("LLLL")}
        </span>
      </div>
      <div className="blogitem-content">
        <h5>
          <a href="blog-details.html">{blog.title}</a>
        </h5>
        <p>{stripHtml(blog.content)}</p>
        <button
          className="btn btn-info"
          onClick={() =>
            props.actions.handleCurBlog({ type: "updateBlog", data: blog })
          }
        >
          Chỉnh sửa
        </button>
        <button
          className="btn btn-danger"
          onClick={() =>
            props.actions.toggleModal({ type: "delBlogModal", data: blog })
          }
        >
          Xoá
        </button>
      </div>
    </div>
  );
};

export default BlogItem;
