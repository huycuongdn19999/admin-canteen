import React, { Component } from "react";
import { Container, Row } from "reactstrap";
import ModalConfirmDel from "../../common/ModalConfirmDel";
import ModalConfirmCancel from "../../common/ModalConfirmDel";
import Header from "../../common/Header";
import BlogEditor from "./BlogEditor";
import BlogList from "./BlogList";
import classnames from "classnames";

class BlogPage extends Component {
  handleViewListBlogs = () => {
    const { isUpdate } = this.props;
    if (isUpdate) {
      this.props.actions.toggleModal({
        type: "cancelBlogEdit"
      });
    } else {
      this.props.actions.handleTabChange({ type: "2" });
    }
  };

  render() {
    const { activedTab, delBlogModal, cancelBlogEdit, curBlog } = this.props;
    return (
      <React.Fragment>
        <div className="block-header">
          <div className="block-header__container">
            <Header
              title={"Danh sách bài viết"}
              subTitle={"Bài viết"}
              active={"Danh sách"}
            />
            <div className="block-header__right">
              <button
                className={classnames("btn btn-lime btn-icon", {
                  active: activedTab === "1"
                })}
                type="button"
                onClick={() =>
                  this.props.actions.handleTabChange({ type: "1" })
                }
              >
                <i className="zmdi zmdi-plus"></i>
              </button>
              <button
                className={classnames("btn btn-lime btn-icon", {
                  active: activedTab === "2"
                })}
                type="button"
                onClick={this.handleViewListBlogs}
              >
                <i className="zmdi zmdi-view-web"></i>
              </button>
            </div>
          </div>
        </div>

        <Container fluid={true}>
          <Row>
            {activedTab === "1" ? (
              <BlogEditor {...this.props} />
            ) : (
              <BlogList {...this.props} />
            )}
          </Row>
        </Container>
        <ModalConfirmDel
          open={delBlogModal}
          toggleModal={() =>
            this.props.actions.toggleModal({
              type: "delBlogModal"
            })
          }
          title={"Xác nhận xoá bài viết"}
          desc="Bạn có chắc chắn xoá bài viết này"
          actions={() => {
            this.props.actions.delBlog({ blogId: curBlog._id });
            this.props.actions.toggleModal({
              type: "delBlogModal"
            });
          }}
        />
        <ModalConfirmCancel
          open={cancelBlogEdit}
          toggleModal={() =>
            this.props.actions.toggleModal({
              type: "cancelBlogEdit"
            })
          }
          title={"Chỉnh sửa bài viết"}
          desc="Bạn có chắc chắn huỷ thao tác"
          actions={() => {
            this.props.actions.handleTabChange({ type: "2" });
            this.props.actions.toggleModal({
              type: "cancelBlogEdit"
            });
          }}
        />
      </React.Fragment>
    );
  }
}

export default BlogPage;
