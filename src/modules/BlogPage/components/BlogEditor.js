import React, { Component } from "react";
import { FormGroup, FormFeedback, Label, Input, Col } from "reactstrap";
import imageCompression from "browser-image-compression";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import _ from "lodash";

class BlogEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputFile: ""
    };
  }

  modules = {
    toolbar: {
      container: [
        [{ header: "1" }, { header: "2" }, { font: [] }, { align: [] }],
        [{ size: [] }],
        ["bold", "italic", "underline", "strike", "blockquote"],
        [
          { list: "ordered" },
          { list: "bullet" },
          { indent: "-1" },
          { indent: "+1" }
        ]
      ]
    },
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false
    }
  };

  formats = [
    "header",
    "font",
    "align",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent"
  ];

  handleValidate = () => {
    const { data } = this.props;
    const { content, title } = this.props.data;

    if (
      _.isEmpty(title) &&
      _.isEmpty(content) &&
      !data.hasOwnProperty("image")
    ) {
      this.props.actions.handleValidate();
      return false;
    } else {
      if (_.isEmpty(title)) {
        this.props.actions.handleValidate("title");
        return false;
      } else if (!data.hasOwnProperty("image")) {
        this.props.actions.handleValidate("image");
        return false;
      } else if (_.isEmpty(content)) {
        this.props.actions.handleValidate("content");
        return false;
      } else return true;
    }
  };

  handlePostBlog = () => {
    const { isUpdate, data, curBlog } = this.props;
    this.setState({
      inputFile: ""
    });
    if (this.handleValidate()) {
      if (isUpdate) {
        this.props.actions.updateBlog({
          ...data,
          blogId: curBlog._id
        });
      } else {
        this.props.actions.postBlog(this.props.data);
        this.props.actions.handleClear({ type: "POST" });
      }
      this.setState({
        inputFile: "1"
      });
    }
  };

  handleEditorOnChange = html => {
    this.props.actions.handleContentEditor(html);
  };

  async _handleImageCompress(imagesUpload) {
    var options = {
      maxSizeMB: 0.3,
      maxWidthOrHeight: 1920
    };
    const imageFile = imagesUpload;
    console.log(imagesUpload);
    const compressedFile = await imageCompression(imageFile, options);
    var newFile = new File([compressedFile], `thumbnail.png`);

    this.props.actions.handleFileChange(newFile);
  }

  render() {
    const { isErrorTitle, isErrorThumb, isErrorContent } = this.props;
    const { content, title } = this.props.data;

    return (
      <React.Fragment>
        <Col lg="12">
          <div className="card">
            <div className="body">
              <FormGroup>
                <Label>Tiêu đề Blog</Label>
                <Input
                  type="text"
                  name="title"
                  placeholder="Nhập tiêu đề bài viết"
                  maxLength="100"
                  value={title}
                  invalid={isErrorTitle}
                  onChange={e => this.props.actions.handleInputChange(e)}
                />
                <FormFeedback>Vui lòng không để trống</FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label for="imageInput">Thumnail</Label>
                <Input
                  id="imageInput"
                  name="image"
                  type="file"
                  accept="image/x-png,image/gif,image/jpeg"
                  invalid={isErrorThumb}
                  key={this.state.inputFile}
                  onChange={e => this._handleImageCompress(e.target.files[0])}
                />
                <FormFeedback>Vui lòng không để trống</FormFeedback>
              </FormGroup>
            </div>
          </div>
          <div className="card">
            <div className="body">
              <FormGroup>
                <Label>Nội dung Blog</Label>
                <ReactQuill
                  onChange={this.handleEditorOnChange}
                  value={content || ""}
                  modules={this.modules}
                  formats={this.formats}
                />
                <Input type="hidden" invalid={isErrorContent} />
                <FormFeedback>Vui lòng không để trống</FormFeedback>
              </FormGroup>
              <button
                type="button"
                className="btn btn-info waves-effect"
                onClick={this.handlePostBlog}
              >
                SUBMIT
              </button>
            </div>
          </div>
        </Col>
      </React.Fragment>
    );
  }
}

export default BlogEditor;
