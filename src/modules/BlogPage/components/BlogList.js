import React, { Component } from "react";
import { Col } from "reactstrap";
import BlogItem from "./BlogItem";
import Pagination from "../../common/Pagination";
import LoadingScreen from "../../common/LoadingScreen";

class BlogList extends Component {
  componentDidMount() {
    const { limitItemPerPage, curPage } = this.props;
    this.props.actions.getAllBlogs({
      limit: limitItemPerPage,
      page: curPage
    });
  }

  render() {
    const { listBlogs, maxPage, isLoading } = this.props;
    return (
      <React.Fragment>
        <Col lg="12">
          {isLoading ? (
            <LoadingScreen />
          ) : (
            <div className="card m-t-10">
              <div className="blog_grid__container">
                {listBlogs.map((blog, index) => {
                  return (
                    <BlogItem
                      key={`blog__${index}`}
                      props={this.props}
                      blog={blog}
                    />
                  );
                })}
              </div>
            </div>
          )}

          {maxPage > 1 && <Pagination {...this.props} />}
        </Col>
      </React.Fragment>
    );
  }
}

export default BlogList;
